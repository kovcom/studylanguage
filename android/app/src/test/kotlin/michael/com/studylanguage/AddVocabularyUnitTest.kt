package michael.com.studylanguage

import android.accounts.NetworkErrorException
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Observable
import michael.com.studylanguage.R.string.translation
import michael.com.studylanguage.translation.Translation
import michael.com.studylanguage.translation.management.AddToVocabularyPresenter
import michael.com.studylanguage.translation.management.AddToVocabularyView
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Spy
import org.mockito.junit.MockitoJUnitRunner
import java.lang.IllegalArgumentException
import java.util.*

/**
 * Created on 05.11.2017.
 */
@RunWith(MockitoJUnitRunner::class)
class AddVocabularyUnitTest {

    @Spy private lateinit var view: AddToVocabularyView

    @Test
    fun `correct adding`() {
        val translation = Translation(0, "Test", "Тест", Date())
        val testTranslationRepository = mock<TranslationRepository>
        {
            on(it.add(any())).doReturn(Observable.just(translation))
        }
        val presenter = AddToVocabularyPresenter(view, testTranslationRepository, TestSchedulersFactory())

        presenter.addToVocabulary(translation)

        Mockito.verify(testTranslationRepository).add(translation)
        val inOrder = inOrder(view)
        inOrder.verify(view).startProgress()
        inOrder.verify(view).translationAdded(translation)
        inOrder.verify(view).endProgress()
    }

    @Test
    fun `exception while adding`() {
        val translation = Translation(0, "Test", "Тест", Date())
        val testTranslationRepository = mock<TranslationRepository>
        {
            on(it.add(any())).doThrow(IllegalArgumentException("Unknown host exception"))
        }
        val presenter = AddToVocabularyPresenter(view, testTranslationRepository, TestSchedulersFactory())

        presenter.addToVocabulary(translation)

        Mockito.verify(testTranslationRepository).add(translation)
        val inOrder = inOrder(view)
        inOrder.verify(view).startProgress()
        inOrder.verify(view).endProgress()
        inOrder.verify(view).showError("Unknown host exception")
    }

    @Test
    fun `destroy before start`()
    {
        val translation = Translation(0, "Test", "Тест", Date())
        val testTranslationRepository = mock<TranslationRepository>{}
        val presenter = AddToVocabularyPresenter(view, testTranslationRepository, TestSchedulersFactory())
        presenter.destroy()
        presenter.addToVocabulary(translation)
    }


}