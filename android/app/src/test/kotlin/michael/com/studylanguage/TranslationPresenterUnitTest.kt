package michael.com.studylanguage

import com.nhaarman.mockito_kotlin.*
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import michael.com.studylanguage.translation.Translation
import michael.com.studylanguage.translation.TranslationPresenter
import michael.com.studylanguage.translation.TranslationView
import michael.com.studylanguage.translation.remote.google.GoogleTranslationEntity
import michael.com.studylanguage.translation.remote.google.GoogleTranslationResponseEntity
import michael.com.studylanguage.translation.remote.google.TranslationService
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Spy
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class TranslationPresenterUnitTest {

    @Spy private lateinit var view: TranslationView

    private val fakeResponse: GoogleTranslationResponseEntity
        get() {
            val item = GoogleTranslationResponseEntity()
            val translations = ArrayList<GoogleTranslationResponseEntity.Translation>()
            val translation = GoogleTranslationResponseEntity.Translation()
            translation.detectedSourceLanguage = "en"
            translation.translatedText = TRANSLATION_RESULT
            translations.add(translation)
            val data = GoogleTranslationResponseEntity.Data()
            data.translations = translations
            item.data = data
            return item
        }

    @Test
    fun `exception while translation`() {
        val incorrect = GoogleTranslationEntity(TRANSLATION, "ua")
        val service = mock<TranslationService> {
            on(it.translateWords(eq(incorrect), any())).
                    doThrow(IllegalArgumentException("No such locale"))
        }

        val translationPresenter = TranslationPresenter(view, FAKE_KEY, service, TestSchedulersFactory())

        translationPresenter.start(incorrect)

        Mockito.verify(service, Mockito.atLeastOnce()).translateWords(incorrect, FAKE_KEY)

        //verify callbacks are invoked
        Mockito.verify<TranslationView>(view).startProgress()
        Mockito.verify<TranslationView>(view).showError("No such locale")
        Mockito.verify<TranslationView>(view).endProgress()
    }


    @Test
    fun `destroy before start`() {
        val incorrect = GoogleTranslationEntity(TRANSLATION, "ua")
        val service = mock<TranslationService> {}
        val view = EmptyTranslationView()
        val presenter = TranslationPresenter(view, "key", service, TestSchedulersFactory())
        presenter.destroy()
        presenter.start(incorrect)
    }

    @Test
    fun `successful translation`() {
        val service = mock<TranslationService>
        {
            on { it.translateWords(any(), any()) }.doReturn(Observable.just(fakeResponse))
        }

        var invocationOrder = 0

        val view = object : EmptyTranslationView() {
            override fun displayTranslation(translation: Translation) {
                val result = Translation(0, TRANSLATION, TRANSLATION_RESULT, Date())
                Assert.assertEquals(translation.origin, result.origin)
                Assert.assertEquals(translation.translation, result.translation)
                Assert.assertEquals(invocationOrder++, 1)
            }

            override fun startProgress() {
                Assert.assertEquals(invocationOrder++, 0)
            }

            override fun endProgress() {
                Assert.assertEquals(invocationOrder, 2)
            }
        }
        val translationPresenter = TranslationPresenter(view, FAKE_KEY, service, TestSchedulersFactory())
        val value = GoogleTranslationEntity(TRANSLATION, "uk")

        translationPresenter.start(value)

        Mockito.verify(service, Mockito.atLeastOnce()).translateWords(value, FAKE_KEY)

        //Assert all methods are invoked
        Assert.assertEquals(2, invocationOrder)
    }


    internal inner class TestSchedulersFactory : SchedulersFactory() {

        override fun getBackgroundScheduler(): Scheduler {
            return Schedulers.trampoline()
        }

        override fun getMainScheduler(): Scheduler {
            return Schedulers.trampoline()
        }

    }

    companion object {

        val FAKE_KEY = "fakeKey"
        val TRANSLATION = "test"
        val TRANSLATION_RESULT = "тест"
    }

    open class EmptyTranslationView : TranslationView {
        override fun setPresenter(presenter: TranslationPresenter) {
        }

        override fun showError(error: String?) {
        }

        override fun startProgress() {
        }

        override fun endProgress() {
        }

        override fun displayTranslation(translation: Translation) {
        }
    }
}
