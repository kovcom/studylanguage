package michael.com.studylanguage

import com.nhaarman.mockito_kotlin.*
import io.reactivex.Observable
import michael.com.studylanguage.translation.Translation
import michael.com.studylanguage.vocabulary.VocabularyPresenter
import michael.com.studylanguage.vocabulary.VocabularyView
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Spy
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

/**
 * Created on 05.11.2017.
 */
@RunWith(MockitoJUnitRunner::class)
class VocabularyPresenterTest {
    @Spy private lateinit var view: VocabularyView

    @Test
    fun `correct retrieving all`() {
        val translations = listOf(Translation(1, "Test", "Тест", Date()),
                Translation(2, "Translation", "Переклад", Date()), Translation(3, "Word", "Слово", Date()))
        val testTranslationRepository = mock<TranslationRepository>
        {
            on(it.getAll()).doReturn(Observable.fromIterable(translations))
        }
        val presenter = VocabularyPresenter(view, testTranslationRepository, TestSchedulersFactory())

        presenter.start()

        Mockito.verify(testTranslationRepository).getAll()
        val inOrder = inOrder(view)
        inOrder.verify(view).startProgress()
        inOrder.verify(view, times(3)).translationRetrieved(any())
        inOrder.verify(view).endProgress()
    }

    @Test
    fun `destroy before start`()
    {
        val testTranslationRepository = mock<TranslationRepository>{}
        val presenter = VocabularyPresenter(view, testTranslationRepository, TestSchedulersFactory())
        presenter.destroy()
        presenter.start()
    }


}