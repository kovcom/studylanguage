package michael.com.studylanguage

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

/**
 * Created on 05.11.2017.
 */
class TestSchedulersFactory : SchedulersFactory() {

    override fun getBackgroundScheduler(): Scheduler {
        return Schedulers.trampoline()
    }

    override fun getMainScheduler(): Scheduler {
        return Schedulers.trampoline()
    }

}