package michael.com.studylanguage

import android.content.Context
import dagger.Module
import dagger.Provides
import michael.com.studylanguage.util.CredentialsProvider

/**
 * Created on 05.11.2017.
 */
@Module
class ApplicationModule(private val context: Context) {

    @Provides
    internal fun providePrefs(): Prefs {
        return Prefs(context)
    }

    @Provides
    fun provideCredentialsProvider(): CredentialsProvider {
        return CredentialsProvider(context)
    }

    @Provides
    fun providesKey(): String {
        return "AIzaSyCGHRA3Dm82chqQRJZQy2IoR_DIWj9SAJ4"
    }

    @Provides
    fun provideScheduleFactory(): SchedulersFactory {
        return TestSchedulersFactory()
    }
}