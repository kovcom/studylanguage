package michael.com.studylanguage

import io.reactivex.Observable
import michael.com.studylanguage.translation.Translation
import michael.com.studylanguage.translation.TranslationDataSourceFactory

/**
 * Created on 05.11.2017.
 */

class FakeTranslationRepository(factory: TranslationDataSourceFactory) : TranslationRepository(factory) {
    val items = mutableListOf<Translation>()
    override fun add(translation: Translation): Observable<Translation> {
        val index = items.indexOf(translation)
        if (index >= 0) {
            items[index] = translation
        }
        return Observable.just(translation)
    }

    override fun getAll(): Observable<Translation> {
        return Observable.fromIterable(items)
    }

    override fun getById(id: Int): Observable<Translation> {
        return Observable.just(items.find { x -> x.id == id })
    }

    override fun getWithText(text: String): Observable<Translation> {
        return Observable.just(items.find { x -> x.origin == text })
    }

    override fun update(newvalue: Translation) {
        val index = items.indexOf(newvalue)
        if (index >= 0) {
            items[index] = newvalue
        }
    }

    override fun removeAll() {
        items.clear()
    }

    override fun remove(id: Int) {
        items.removeIf { x -> x.id == id }
    }
}