package michael.com.studylanguage.vocabulary

import android.arch.persistence.room.Room
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_vocabulary.*
import michael.com.studylanguage.ApplicationModule
import michael.com.studylanguage.R
import michael.com.studylanguage.api.ServiceGenerator
import michael.com.studylanguage.translation.TranslationDataSource
import michael.com.studylanguage.translation.TranslationDataSourceFactory
import michael.com.studylanguage.translation.TranslationModule
import michael.com.studylanguage.translation.local.LocalTranslationDataSource
import michael.com.studylanguage.translation.local.TranslationDatabase
import michael.com.studylanguage.translation.remote.RemoteTranslationDataSource
import michael.com.studylanguage.translation.remote.google.TranslationService
import michael.com.studylanguage.util.C
import javax.inject.Inject

class VocabularyActivity : AppCompatActivity() {


    @Inject lateinit var presenter: VocabularyPresenter

    private var vocabularyFragment: VocabularyFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vocabulary)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (vocabularyFragment == null) {
            vocabularyFragment = supportFragmentManager.findFragmentById(R.id.fragment) as VocabularyFragment?
        }
        initDaggerComponents()
    }

    private fun initDaggerComponents() {
        val translationService = ServiceGenerator().getObservableService(TranslationService::class.java, Gson())
        val factory = object : TranslationDataSourceFactory {
            override fun provideLocalDataSource(): TranslationDataSource {
                val db = Room.databaseBuilder(applicationContext, TranslationDatabase::class.java, C.DB_NAME).build()
                return LocalTranslationDataSource(db.translationDao())
            }

            override fun provideRemoteDataSource(): TranslationDataSource {
                return RemoteTranslationDataSource()
            }
        }
        DaggerVocabularyComponent.builder()
                .applicationModule(ApplicationModule(this))
                .vocabularyModule(VocabularyModule(vocabularyFragment!!))
                .translationModule(TranslationModule(null, translationService, factory))
                .build().inject(this)
    }

}
