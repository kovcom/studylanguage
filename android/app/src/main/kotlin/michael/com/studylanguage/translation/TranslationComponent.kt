package michael.com.studylanguage.translation

import dagger.Component
import michael.com.studylanguage.ApplicationModule
import michael.com.studylanguage.translation.management.TranslationManagementModule
import michael.com.studylanguage.util.FragmentScoped

/**
 * @author Michael
 * created on 04.10.2016.
 */
@Component(modules = arrayOf(ApplicationModule::class, TranslationModule::class, TranslationManagementModule::class))
@FragmentScoped
interface TranslationComponent {
    fun inject(activity: TranslationActivity)
}