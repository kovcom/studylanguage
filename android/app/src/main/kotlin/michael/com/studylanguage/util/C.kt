package michael.com.studylanguage.util

/**
 * Constants
 *
 * Created on 02.11.2017.
 */
object C {
    const val DB_NAME = "vocabulary.db"
    const val URL:String = "https://translation.googleapis.com/"
}
