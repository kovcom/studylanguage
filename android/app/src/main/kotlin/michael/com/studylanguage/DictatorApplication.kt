package michael.com.studylanguage

import android.app.Application

/**
 * @author Michael
 * *         created on 21.08.2016.
 */
class DictatorApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        application = this
    }

    companion object {
        private lateinit var application: DictatorApplication
        val TAG = DictatorApplication.javaClass.simpleName
        fun get(): DictatorApplication {
            return application
        }
    }
}
