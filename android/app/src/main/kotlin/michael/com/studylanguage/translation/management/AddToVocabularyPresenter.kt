package michael.com.studylanguage.translation.management

import io.reactivex.disposables.Disposable
import michael.com.studylanguage.DefaultErrorSubscriber
import michael.com.studylanguage.SchedulersFactory
import michael.com.studylanguage.TranslationRepository
import michael.com.studylanguage.translation.AddToVocabularyUseCase
import michael.com.studylanguage.translation.Translation
import javax.inject.Inject

/**
 * Created on 02.11.2017.
 */
class AddToVocabularyPresenter
@Inject constructor(val addToVocabularyView: AddToVocabularyView?, private val repository: TranslationRepository,
                    private val schedulersFactory: SchedulersFactory) {
    init {
        addToVocabularyView?.setPresenter(this)
    }

    private var useCase: AddToVocabularyUseCase? = null

    fun addToVocabulary(translation: Translation) {
        useCase?.unsubscribe()
        useCase = AddToVocabularyUseCase(schedulersFactory.getBackgroundScheduler(),
                schedulersFactory.getMainScheduler(), repository, translation)
        useCase?.execute(AddToVocabularySubscriber())
    }

    private inner class AddToVocabularySubscriber : DefaultErrorSubscriber<Translation>(addToVocabularyView!!,
            AddToVocabularySubscriber::class.java.simpleName) {
        override fun onSubscribe(d: Disposable) {
            addToVocabularyView?.startProgress()
        }

        override fun onComplete() {
            addToVocabularyView?.endProgress()
        }

        override fun onNext(t: Translation) {
            addToVocabularyView?.translationAdded(t)
        }


        override fun onError(e: Throwable) {
            addToVocabularyView?.endProgress()
            super.onError(e)
        }
    }

    fun destroy() {
        useCase?.unsubscribe()
    }
}