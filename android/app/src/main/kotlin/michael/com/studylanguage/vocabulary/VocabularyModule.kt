package michael.com.studylanguage.vocabulary

import dagger.Module
import dagger.Provides

/**
 * Created on 02.11.2017.
 */
@Module
class VocabularyModule(val view: VocabularyView) {
    @Provides
    fun provideVocabularyView(): VocabularyView {
        return view
    }
}