package michael.com.studylanguage.translation

import io.reactivex.disposables.Disposable
import michael.com.studylanguage.BasePresenter
import michael.com.studylanguage.DefaultErrorSubscriber
import michael.com.studylanguage.SchedulersFactory
import michael.com.studylanguage.translation.remote.google.GoogleTranslationEntity
import michael.com.studylanguage.translation.remote.google.TranslationService
import retrofit2.HttpException
import javax.inject.Inject

/**
 * @author Michael
 * created on 25.09.2016.
 */
class TranslationPresenter
@Inject constructor(val translationView: TranslationView,
                    val key: String, val service: TranslationService, val schedulersFactory: SchedulersFactory) : BasePresenter {
    private var useCase: TranslationUseCase? = null

    init {
        translationView.setPresenter(this)
    }

    fun start(data: GoogleTranslationEntity) {
        useCase = TranslationUseCase(data, service, key, schedulersFactory.getBackgroundScheduler(), schedulersFactory.getMainScheduler())
        start()
    }

    override fun destroy() {
        useCase?.unsubscribe()
    }

    override fun start() {
        translationView.startProgress()
        useCase?.execute(TranslationSubscriber())
    }

    private inner class TranslationSubscriber : DefaultErrorSubscriber<Translation>(translationView,
            TranslationSubscriber::class.java.simpleName) {
        override fun onSubscribe(d: Disposable) {
        }

        override fun onComplete() {
            translationView.endProgress()
        }

        override fun onNext(t: Translation) {
            translationView.displayTranslation(t!!)
        }

        override fun onError(e: Throwable) {
            translationView.endProgress()
            if (e is HttpException) {
                if (e.code() == 401) {
                    useCase?.execute(this)
                    return
                }
            }
            super.onError(e)
        }
    }
}
