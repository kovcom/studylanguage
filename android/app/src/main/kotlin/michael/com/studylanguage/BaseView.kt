package michael.com.studylanguage

interface BaseView<T> {

    fun setPresenter(presenter: T)

}
