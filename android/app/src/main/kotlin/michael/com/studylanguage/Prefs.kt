package michael.com.studylanguage

import android.content.Context
import android.content.SharedPreferences

/**
 * @author Michael
 * created on 04.10.2016.
 */
class Prefs(context: Context) {

    companion object {
        val L = "extra_l"
        val P = "extra_p"
    }

    private val sharedPreferences: SharedPreferences = context.getSharedPreferences("test", Context.MODE_PRIVATE)

    /**
     * returns "" if no previous values was entered
     */
    var l: String get() {
        return sharedPreferences.getString(L, "")
    }
        set(value) {
            sharedPreferences.edit().putString(L, value).apply()
        }
    /**
     * returns "" if no previous values was entered
     */
    var p: String get() {
        return sharedPreferences.getString(P, "")
    }
        set(value) {
            sharedPreferences.edit().putString(P, value).apply()
        }
}