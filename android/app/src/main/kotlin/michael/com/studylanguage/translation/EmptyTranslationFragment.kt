package michael.com.studylanguage.translation

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_empty_translation.*
import michael.com.studylanguage.R
import michael.com.studylanguage.translation.remote.google.GoogleTranslationEntity
import java.util.*

/**
 * @author Michael
 * created on 04.10.2016.
 */
class EmptyTranslationFragment : Fragment(), TranslationView {
    private var progress: ProgressBar? = null
    private var button: View? = null
    private lateinit var presenter: TranslationPresenter

    companion object {
        private val EXTRA_TRANSLATION = "translation"
        private val EXTRA_ORIGIN = "origin"
        val TAG = EmptyTranslationFragment::class.java.simpleName
    }

    private lateinit var listener: TranslationCommunicationListener

    override fun startProgress() {
        progress?.visibility = View.VISIBLE
        button?.isEnabled = false
    }

    override fun endProgress() {
        progress?.visibility = View.INVISIBLE
        button?.isEnabled = true
    }

    override fun displayTranslation(translation: Translation) {
        translation_result.setText(translation.translation)
        listener.translationReceived(translation)
    }

    override fun setPresenter(presenter: TranslationPresenter) {
        this.presenter = presenter
    }

    override fun showError(error: String?) {
        Toast.makeText(context, error, Toast.LENGTH_LONG).show()
    }

    override fun onAttach(context: Context?) {
        listener = context as TranslationCommunicationListener
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val inflateView = inflater!!.inflate(R.layout.fragment_empty_translation, container, false)
        button = activity.findViewById(R.id.fab)
        button?.setOnClickListener { ok() }
        progress = activity.findViewById(R.id.progressBar)
        return inflateView
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putString(EXTRA_TRANSLATION, translation_result.text.toString())
        outState?.putString(EXTRA_ORIGIN, text_to_translate.text.toString())
        super.onSaveInstanceState(outState)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        translation_result.setText(savedInstanceState?.getString(EXTRA_TRANSLATION))
        text_to_translate.setText(savedInstanceState?.getString(EXTRA_ORIGIN))
    }

    override fun onDestroyView() {
        presenter.destroy()
        super.onDestroyView()
    }
    private fun ok() {
        if (text_to_translate != null && text_to_translate.text.trim().isNotEmpty()) {
            val languageTo = Locale.getDefault().isO3Language

            val data = GoogleTranslationEntity(text_to_translate.text.toString(), languageTo)
            presenter.start(data)
        } else {
            Toast.makeText(context, getString(R.string.error_empty_text), Toast.LENGTH_SHORT).show()
        }
    }
}