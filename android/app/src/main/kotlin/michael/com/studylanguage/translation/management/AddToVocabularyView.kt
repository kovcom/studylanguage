package michael.com.studylanguage.translation.management

import michael.com.studylanguage.BaseView
import michael.com.studylanguage.ErrorView
import michael.com.studylanguage.translation.Translation

/**
 * Created on 02.11.2017.
 */
interface AddToVocabularyView : BaseView<AddToVocabularyPresenter>, ErrorView {
    fun startProgress()
    fun endProgress()
    fun translationAdded(translation: Translation)
}
