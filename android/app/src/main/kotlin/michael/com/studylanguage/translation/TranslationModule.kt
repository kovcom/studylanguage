package michael.com.studylanguage.translation

import dagger.Module
import dagger.Provides
import michael.com.studylanguage.translation.remote.google.TranslationService

/**
 * @author Michael
 * created on 04.10.2016.
 */
@Module
class TranslationModule(val view: TranslationView?, val service: TranslationService, val factory: TranslationDataSourceFactory) {
    @Provides
    fun getTranslationView(): TranslationView {
        return view!!
    }

    @Provides
    fun getTranslationService(): TranslationService {
        return service
    }

    @Provides
    fun getLocalDataStorageFactory(): TranslationDataSourceFactory {
        return factory
    }
}