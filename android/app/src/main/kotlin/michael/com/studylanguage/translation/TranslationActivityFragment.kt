package michael.com.studylanguage.translation

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import michael.com.studylanguage.R
import michael.com.studylanguage.translation.remote.google.GoogleTranslationEntity
import java.util.*

/**
 * A placeholder fragment containing a simple view.
 */
class TranslationActivityFragment : Fragment(), TranslationView {

    private var progress: ProgressBar? = null
    override fun startProgress() {
        progress?.visibility = View.VISIBLE
        button.isEnabled = false
    }

    override fun endProgress() {
        progress?.visibility = View.INVISIBLE
        button.isEnabled = true

    }

    override fun displayTranslation(translation: Translation) {

        Toast.makeText(context,"translation is finished: ${translation.translation}" ,Toast.LENGTH_LONG).show()
    }

    private lateinit var presenter: TranslationPresenter
    override fun setPresenter(presenter: TranslationPresenter) {
        this.presenter = presenter
    }

    override fun showError(error: String?) {
        Toast.makeText(context, error,Toast.LENGTH_LONG).show()
    }

    companion object {
        val EXTRA_CONTEXT = "context"
        val TAG = TranslationActivityFragment::class.java.simpleName
        fun newInstance(context: String): TranslationActivityFragment {
            val instance = TranslationActivityFragment()
            val bundle = Bundle()
            bundle.putString(EXTRA_CONTEXT, context)
            instance.arguments = bundle
            return instance
        }
    }

    private val strings = LinkedList<String>()

    lateinit var viewGroup: LinearLayout
    private lateinit var tempLayout: LinearLayout
    private var view: TextView? = null
    lateinit var button: View

    private fun inflateText(text: String) {
        val words = text.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        Collections.addAll(this.strings, *words)

        if (strings.size > 0) {
            tempLayout = LinearLayout(context)
            tempLayout.orientation = LinearLayout.HORIZONTAL
            tempLayout.setBackgroundColor(resources.getColor(R.color.colorAccent))
            viewGroup.addView(tempLayout)

            addView()
        }
    }

    private fun addView() {
        val view = activity.layoutInflater.inflate(R.layout.text_view, null) as TextView
        view.text = strings.poll() + " "
        view.setOnClickListener {
            view.isSelected = !view.isSelected
            val value = view.text.toString()
            if (selectedItems.contains(value)) {
                selectedItems.remove(value)
            } else {
                selectedItems.add(value)
            }
        }
        this.view = view
        tempLayout.addView(view)
    }

    private var data: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        data = arguments.getString(EXTRA_CONTEXT)
    }

    private lateinit var listener: ViewTreeObserver.OnGlobalLayoutListener

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val inflateView = inflater!!.inflate(R.layout.fragment_translation, container, false)
        viewGroup = inflateView.findViewById<LinearLayout>(R.id.main_layout) as LinearLayout
        listener = ViewTreeObserver.OnGlobalLayoutListener {
            if (strings.size > 0) {
                if (view?.right != null && view!!.right >= tempLayout.measuredWidth) {
                    tempLayout.removeView(view)
                    tempLayout = LinearLayout(activity)
                    tempLayout.setBackgroundColor(resources.getColor(R.color.colorAccent))
                    tempLayout.orientation = LinearLayout.HORIZONTAL
                    tempLayout.addView(view)
                    viewGroup.addView(tempLayout)
                }
                addView()
            }
        }
        viewGroup.viewTreeObserver.addOnGlobalLayoutListener(listener)
        if (data != null) {
            inflateText(text = data!!)
        }
        button = activity.findViewById(R.id.fab)
        button.setOnClickListener { view -> ok() }
        progress = activity.findViewById<ProgressBar>(R.id.progressBar) as ProgressBar?
        return inflateView
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewGroup.viewTreeObserver.removeOnGlobalLayoutListener(listener)
    }

    val selectedItems: ArrayList<String> = ArrayList()
    fun ok() {
        val languageFrom = Locale.ENGLISH.isO3Language
        val languageTo = Locale.getDefault().isO3Language
//        val data = TranslationData(languageFrom, languageTo, "test", if (data != null) data!! else "")
        Log.d(TAG, "Data: $data")
        val data = GoogleTranslationEntity(if (data != null) data!! else "", languageTo)
        presenter.start(data)
    }
}
