package michael.com.studylanguage.vocabulary

import michael.com.studylanguage.BaseView
import michael.com.studylanguage.ErrorView
import michael.com.studylanguage.translation.Translation

/**
 * Created on 02.11.2017.
 */
interface VocabularyView : BaseView<VocabularyPresenter>, ErrorView {
    fun startProgress()
    fun endProgress()
    fun translationRetrieved(translation: Translation)
}