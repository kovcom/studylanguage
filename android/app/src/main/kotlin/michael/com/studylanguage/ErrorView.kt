package michael.com.studylanguage

/**
 * View that can show error and perform action
 */
interface ErrorView {

    /**
     * Show error to user and perform action to resolve it

     * @param error  Error to be shown.
     */
    fun showError(error: String?)
}
