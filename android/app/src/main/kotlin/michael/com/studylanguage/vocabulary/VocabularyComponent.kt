package michael.com.studylanguage.vocabulary

import dagger.Component
import michael.com.studylanguage.ApplicationModule
import michael.com.studylanguage.translation.TranslationModule
import michael.com.studylanguage.util.FragmentScoped

/**
 * Created on 02.11.2017.
 */
@Component(modules = arrayOf(ApplicationModule::class, VocabularyModule::class, TranslationModule::class))
@FragmentScoped
interface VocabularyComponent {
    fun inject(activity: VocabularyActivity)
}