package michael.com.studylanguage.splash

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import michael.com.studylanguage.Prefs
import michael.com.studylanguage.R
import michael.com.studylanguage.translation.TranslationActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        val prefs = Prefs(this)
        // if no login or password are saved - start registration
        if (prefs.l.length > 0 && prefs.p.length > 0) {
            startActivity(TranslationActivity.createIntent(context = this, text = ""))
            finish()
        } else {
            startActivity(TranslationActivity.createIntent(context = this, text = ""))
//            startActivity(Intent(this, RegistrationActivity::class.java))
            finish()
        }
    }
}
