package michael.com.studylanguage.vocabulary

import io.reactivex.disposables.Disposable
import michael.com.studylanguage.BasePresenter
import michael.com.studylanguage.DefaultErrorSubscriber
import michael.com.studylanguage.SchedulersFactory
import michael.com.studylanguage.TranslationRepository
import michael.com.studylanguage.translation.GetListOfWordsUseCase
import michael.com.studylanguage.translation.Translation
import javax.inject.Inject

/**
 * Created on 02.11.2017.
 */
class VocabularyPresenter
@Inject constructor(private val vocabularyView: VocabularyView?, private val repository: TranslationRepository,
                    private val schedulersFactory: SchedulersFactory)
    : BasePresenter {
    override fun destroy() {
        useCase?.unsubscribe()
    }

    override fun start() {
        useCase?.unsubscribe()
        useCase = GetListOfWordsUseCase(schedulersFactory.getBackgroundScheduler(), schedulersFactory.getMainScheduler(), repository)
        useCase?.execute(GetVocabularySubscriber(vocabularyView!!))
    }

    private var useCase: GetListOfWordsUseCase? = null

    init {
        vocabularyView?.setPresenter(this)
    }

    class GetVocabularySubscriber(private val vocabularyView: VocabularyView?) :
            DefaultErrorSubscriber<Translation>(vocabularyView!!, GetVocabularySubscriber::class.java.simpleName) {
        override fun onNext(t: Translation) {
            vocabularyView?.translationRetrieved(t)
        }

        override fun onComplete() {
            vocabularyView?.endProgress()
        }

        override fun onSubscribe(d: Disposable) {
            vocabularyView?.startProgress()
        }
    }

}