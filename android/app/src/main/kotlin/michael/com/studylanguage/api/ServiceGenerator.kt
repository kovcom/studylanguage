package michael.com.studylanguage.api

import com.google.gson.Gson
import michael.com.studylanguage.BuildConfig
import michael.com.studylanguage.util.C
import michael.com.studylanguage.util.LoggingInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * @author Michael
 * created on 18.09.2016.
 */

class ServiceGenerator {
    private val loggingInterceptor: LoggingInterceptor by lazy<LoggingInterceptor>
    {
        val logger = if (BuildConfig.DEBUG) LoggingInterceptor.Level.BODY else LoggingInterceptor.Level.NONE
        LoggingInterceptor(logger)
    }

    fun <S> getObservableService(serviceClass: Class<S>, gson: Gson?): S {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(loggingInterceptor)
        val client = httpClient.build()
        val retrofit = getObservableBuilder(gson).client(client).build()

        return retrofit.create(serviceClass)
    }

    private fun getObservableBuilder(gson: Gson?): Retrofit.Builder {
        val builder = Retrofit.Builder()
                .baseUrl(C.URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        if (gson != null) {
            builder.addConverterFactory(GsonConverterFactory.create(gson))
        } else {
            builder.addConverterFactory(GsonConverterFactory.create())
        }
        return builder
    }
}