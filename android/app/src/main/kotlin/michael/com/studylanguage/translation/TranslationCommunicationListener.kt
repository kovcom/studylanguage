package michael.com.studylanguage.translation

/**
 * Communication between Activity and Fragment
 * Created on 02.11.2017.
 */
interface TranslationCommunicationListener {
    fun translationReceived(translation: Translation)
}