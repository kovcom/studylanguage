package michael.com.studylanguage.util

import android.content.Context
import com.burgstaller.okhttp.digest.Credentials
import michael.com.studylanguage.Prefs
import javax.inject.Inject

/**
 * @author Michael
 * created on 11.10.2016.
 */

class CredentialsProvider
@Inject
constructor(val context: Context) {

    fun get(): Credentials {
        val prefs = Prefs(context)
        return Credentials(prefs.l, prefs.p)
    }
}