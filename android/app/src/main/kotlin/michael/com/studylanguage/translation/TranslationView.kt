package michael.com.studylanguage.translation

import michael.com.studylanguage.BaseView
import michael.com.studylanguage.ErrorView

/**
 * @author Michael
 * created on 25.09.2016.
 */
interface TranslationView : BaseView<TranslationPresenter>, ErrorView {
    fun startProgress()
    fun endProgress()
    fun displayTranslation(translation: Translation)
}
