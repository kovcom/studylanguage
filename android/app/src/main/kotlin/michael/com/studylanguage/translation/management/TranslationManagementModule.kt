package michael.com.studylanguage.translation.management

import dagger.Module
import dagger.Provides

/**
 * Created on 02.11.2017.
 */
@Module
class TranslationManagementModule(val addtoVocabularyView: AddToVocabularyView) {
    @Provides
    fun provideAddToVocabularyView(): AddToVocabularyView {
        return addtoVocabularyView
    }
}