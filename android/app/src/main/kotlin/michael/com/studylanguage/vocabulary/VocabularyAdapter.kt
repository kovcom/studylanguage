package michael.com.studylanguage.vocabulary

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.layout_translation_item.view.*
import michael.com.studylanguage.R
import michael.com.studylanguage.translation.Translation

/**
 * Created on 02.11.2017.
 */
class VocabularyAdapter : RecyclerView.Adapter<VocabularyAdapter.TranslationViewHolder>() {
    val items = mutableListOf<Translation>()

    override fun onBindViewHolder(holder: TranslationViewHolder?, position: Int) {
        val item = items[position]
        holder?.apply {
            original.text = item.origin
            translation.text = item.translation
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): TranslationViewHolder {
        val inflater = LayoutInflater.from(parent!!.context)
        val view = inflater.inflate(R.layout.layout_translation_item, parent, false)
        return TranslationViewHolder(view)
    }


    inner class TranslationViewHolder constructor(v: View) : RecyclerView.ViewHolder(v) {
        val original = v.origin
        val translation = v.translation_result
    }
}