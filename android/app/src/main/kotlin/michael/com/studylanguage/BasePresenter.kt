package michael.com.studylanguage

interface BasePresenter {

    /**
     * Method that control the lifecycle of the translationView. It should be called in the translationView's
     * (Activity or Fragment) onDestroy() method.
     */
    fun destroy()

    /**
     * Method for start active work. Used at most when translationView is available and content become being
     * visible.
     */
    fun start()
}
