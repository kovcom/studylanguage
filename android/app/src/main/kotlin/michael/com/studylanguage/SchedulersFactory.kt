package michael.com.studylanguage

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created on 03.11.2017.
 */
open class SchedulersFactory {
    open fun getBackgroundScheduler() = Schedulers.io()
    open fun getMainScheduler() = AndroidSchedulers.mainThread()
}