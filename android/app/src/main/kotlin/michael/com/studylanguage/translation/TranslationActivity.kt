package michael.com.studylanguage.translation

import android.arch.persistence.room.Room
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import com.google.gson.Gson
import kotlinx.android.synthetic.main.layout_progress.*
import michael.com.studylanguage.ApplicationModule
import michael.com.studylanguage.R
import michael.com.studylanguage.api.ServiceGenerator
import michael.com.studylanguage.translation.local.LocalTranslationDataSource
import michael.com.studylanguage.translation.local.TranslationDatabase
import michael.com.studylanguage.translation.management.AddToVocabularyPresenter
import michael.com.studylanguage.translation.management.AddToVocabularyView
import michael.com.studylanguage.translation.management.TranslationManagementModule
import michael.com.studylanguage.translation.remote.RemoteTranslationDataSource
import michael.com.studylanguage.translation.remote.google.TranslationService
import michael.com.studylanguage.util.C
import michael.com.studylanguage.vocabulary.VocabularyActivity
import javax.inject.Inject


class TranslationActivity : AppCompatActivity(), TranslationCommunicationListener, AddToVocabularyView {
    private var translation: Translation? = null
    private var fragment: TranslationActivityFragment? = null
    private var emptyFragment: EmptyTranslationFragment? = null

    @Inject
    lateinit var addVocabularyPresenter: AddToVocabularyPresenter

    @Inject
    lateinit var translationPresenter: TranslationPresenter

    companion object {
        val EXTRA_CONTEXT = "context"
        fun createIntent(context: Context, text: String): Intent {
            val intent = Intent(context, TranslationActivity::class.java)
            intent.putExtra(EXTRA_CONTEXT, text)
            return intent
        }
    }

    override fun setPresenter(presenter: AddToVocabularyPresenter) {
        addVocabularyPresenter = presenter
    }

    override fun showError(error: String?) {
        if (error != null) {
            Toast.makeText(this, error, Toast.LENGTH_LONG).show()
        }
    }

    override fun startProgress() {
        progressBar?.visibility = VISIBLE
    }

    override fun endProgress() {
        progressBar?.visibility = GONE
    }

    override fun translationAdded(translation: Translation) {
        Toast.makeText(this, getString(R.string.message_translation_added), Toast.LENGTH_LONG).show()
    }

    override fun translationReceived(translation: Translation) {
        this.translation = translation
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_translation)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        var text: String? = null
        if (intent?.clipData?.itemCount != null && intent.clipData.itemCount > 0) {
            text = intent.clipData.getItemAt(0).text.toString()
        }
        if (text == null && intent.extras != null) {
            text = intent.extras.getString(EXTRA_CONTEXT)
        }

        if (text != null && text.isNotEmpty()) {
            fragment = supportFragmentManager.findFragmentByTag(TranslationActivityFragment.TAG) as TranslationActivityFragment?
            if (fragment == null) {
                fragment = TranslationActivityFragment.newInstance(text)
                val transaction = supportFragmentManager.beginTransaction()
                transaction.replace(R.id.main_content, fragment, TranslationActivityFragment.TAG)
                transaction.commit()
            }
        } else {
            emptyFragment = supportFragmentManager.findFragmentByTag(EmptyTranslationFragment.TAG) as EmptyTranslationFragment?
            if (emptyFragment == null) {
                emptyFragment = EmptyTranslationFragment()
                val transaction = supportFragmentManager.beginTransaction()
                transaction.replace(R.id.main_content, emptyFragment, EmptyTranslationFragment.TAG)
                transaction.commit()
            }
        }
        initDaggerComponents()
    }

    private fun initDaggerComponents() {
        val translationService = ServiceGenerator().getObservableService(TranslationService::class.java, Gson())

        val factory = object : TranslationDataSourceFactory {
            override fun provideLocalDataSource(): TranslationDataSource {
                val db = Room.databaseBuilder(applicationContext, TranslationDatabase::class.java, C.DB_NAME).build()
                return LocalTranslationDataSource(db.translationDao())
            }

            override fun provideRemoteDataSource(): TranslationDataSource {
                return RemoteTranslationDataSource()
            }
        }
        DaggerTranslationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .translationManagementModule(TranslationManagementModule(this))
                .translationModule(TranslationModule(if (emptyFragment != null) emptyFragment!! else fragment!!, translationService, factory))
                .build().inject(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_translation, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_add -> maybeAddTranslation()
            R.id.action_vocabulary -> startVocabularyActivity()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        addVocabularyPresenter.destroy()
        super.onDestroy()
    }

    private fun startVocabularyActivity() {
        startActivity(Intent(this, VocabularyActivity::class.java))
    }

    private fun maybeAddTranslation() {
        if (translation == null) {
            Toast.makeText(this, getString(R.string.error_no_translation_available), Toast.LENGTH_LONG).show()
        } else {
            addVocabularyPresenter.addToVocabulary(translation!!)
        }
    }

}
