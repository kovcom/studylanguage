package michael.com.studylanguage.vocabulary

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.layout_progress.*
import michael.com.studylanguage.R
import michael.com.studylanguage.translation.Translation

/**
 * A placeholder fragment containing a simple view.
 */
class VocabularyFragment : Fragment(), VocabularyView {
    private val vocabularyAdapter = VocabularyAdapter()
    private lateinit var presenter: VocabularyPresenter
    override fun setPresenter(presenter: VocabularyPresenter) {
        this.presenter = presenter
        presenter.start()
    }

    override fun showError(error: String?) {
        Toast.makeText(context, error, Toast.LENGTH_LONG).show()
    }

    override fun startProgress() {
        progressBar?.visibility = VISIBLE
    }

    override fun endProgress() {
        progressBar?.visibility = GONE
    }

    override fun translationRetrieved(translation: Translation) {
        vocabularyAdapter.apply {
            items.add(translation)
            notifyItemInserted(items.size - 1)
        }
    }

    override fun onDestroyView() {
        presenter.destroy()
        super.onDestroyView()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_vocabulary, container, false)
        val vocabulary = view.findViewById<RecyclerView>(R.id.vocabulary)
        vocabulary.layoutManager = LinearLayoutManager(context)
        vocabulary.adapter = vocabularyAdapter
        return view
    }
}
