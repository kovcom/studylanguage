package michael.com.studylanguage

import android.content.Context
import android.util.Log

/**
 * @author Michael
 * created on 02.10.2016.
 */
class ErrorHandler {
    companion object {
        /**
         * It can return null if incoming error is null or if message is null
         */
        fun getUserFriendlyError(context: Context, error: Throwable?, fullMessage: Boolean): String? {
            if (fullMessage) {
                return error?.message
            }
            return context.getString(R.string.general_error)
        }

        fun handleException(tag: String, ex: Throwable?) {
            if (ex != null) {
                ex.printStackTrace()
                Log.e(tag, ex.message)
            } else {
                Log.e(tag, "Empty Exception")
            }

        }
    }
}