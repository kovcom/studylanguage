package michael.com.studylanguage

import android.util.Log
import io.reactivex.Observer

/**
 * @author Michael
 * created on 02.10.2016.
 */

abstract class DefaultErrorSubscriber<T>(val view: ErrorView, val tag: String = "DefaultErrorSubscriber") : Observer<T> {
    override fun onError(e: Throwable) {
        e.printStackTrace()
        Log.e(tag, e.message)
        view.showError(e.message)
    }
}