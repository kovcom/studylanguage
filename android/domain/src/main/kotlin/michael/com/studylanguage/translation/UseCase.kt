package michael.com.studylanguage.translation

import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable

/**
 * @author Michael
 * created on 24.09.2016.
 */
abstract class UseCase<T> protected constructor(private val scheduler: Scheduler, private val postExecutionThread: Scheduler) {

    private var subscription: Disposable? = null
    private val TAG = "UseCase"

    /**
     * Builds an Observable which will be used when executing the current [UseCase].
     */
    protected abstract fun buildUseCaseObservable(): Observable<T>

    private var useCase: Observable<T>? = null
    /**
     * Executes the current use case.

     * @param useCaseSubscriber The guy who will be listen to the observable build
     * *                          with [.buildUseCaseObservable].
     */
    @SuppressWarnings("unchecked")
    fun execute(useCaseSubscriber: Observer<T>) {
        if (useCase == null) {
            useCase = this.buildUseCaseObservable()

            val internalObserver: Observer<T> = object : Observer<T> {
                override fun onSubscribe(d: Disposable) {
                    subscription = d
                    useCaseSubscriber.onSubscribe(d)
                }

                override fun onComplete() {
                    useCaseSubscriber.onComplete()
                }

                override fun onError(e: Throwable) {
                    useCaseSubscriber.onError(e)
                }

                override fun onNext(t: T) {
                    useCaseSubscriber.onNext(t)
                }
            }
            useCase?.subscribeOn(scheduler)
                    ?.observeOn(postExecutionThread)
                    ?.subscribe(internalObserver)
        }
    }

    /**
     * Unsubscribes from current Subscription.
     */
    open fun unsubscribe() {
        if (!subscription?.isDisposed!!) {
            subscription?.dispose()
        }
    }
}