package michael.com.studylanguage.translation

import io.reactivex.Observable
import io.reactivex.Scheduler
import michael.com.studylanguage.TranslationRepository

/**
 * Created on 02.11.2017.
 */
class AddToVocabularyUseCase(scheduler: Scheduler, postExecutionThread: Scheduler,
                             private val repository: TranslationRepository, val translation: Translation) : UseCase<Translation>(scheduler, postExecutionThread) {
    override fun buildUseCaseObservable(): Observable<Translation> {
        return Observable.defer { repository.add(translation).toObservable() }
    }
}