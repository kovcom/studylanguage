package michael.com.studylanguage.translation

import io.reactivex.Observable
import io.reactivex.Scheduler
import michael.com.studylanguage.translation.remote.google.GoogleTranslationEntity
import michael.com.studylanguage.translation.remote.google.TranslationService
import java.util.*


/**
 * @author Michael
 * created on 25.09.2016.
 */
class TranslationUseCase(private val data: GoogleTranslationEntity, val service: TranslationService,
                         val key: String, val bgScheduler: Scheduler, val postScheduler: Scheduler) : UseCase<Translation>
(bgScheduler, postScheduler) {
    override fun buildUseCaseObservable(): Observable<Translation> {
        return Observable.defer {
            service.translateWords(data, key).map { response ->
                Translation(0, data.q, response.data?.translations?.get(0)?.translatedText, Date())
            }
        }
    }
}
