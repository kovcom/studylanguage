package michael.com.studylanguage.data

import com.nhaarman.mockito_kotlin.*
import io.reactivex.Observable
import michael.com.studylanguage.TranslationRepository
import michael.com.studylanguage.translation.Translation
import michael.com.studylanguage.translation.TranslationDataSource
import michael.com.studylanguage.translation.TranslationDataSourceFactory
import org.junit.Assert
import org.junit.Assert.fail
import org.junit.Test
import java.util.*

/**
 * Tests for {@link TranslationRepository}
 */
class TranslationRepositoryTest {
    @Test
    @Throws(Exception::class)
    fun `get all from empty database`() {
        val localDataSource = mock<TranslationDataSource> {
            on(it.getAll()).doReturn(Observable.empty())
        }
        val remoteDataSource = mock<TranslationDataSource> {
            on(it.getAll()).doReturn(Observable.fromIterable(fakeTranslations))
        }

        val repository = TranslationRepository(FakeTranslationRepositoryFactory(localDataSource, remoteDataSource))
        val values = repository.getAll()
        values.count().subscribe({ count ->
            Assert.assertEquals(fakeTranslations.size.toLong(), count)
            Assert.assertEquals(false, repository.isCacheDirty)
            verify(remoteDataSource).getAll()
            verify(localDataSource, times(fakeTranslations.size)).add(any())
        }, { fail() })
    }

    class FakeTranslationRepositoryFactory(private val localDataSource: TranslationDataSource,
                                           private val remoteTranslationDataSource: TranslationDataSource) : TranslationDataSourceFactory {
        override fun provideLocalDataSource(): TranslationDataSource = localDataSource

        override fun provideRemoteDataSource(): TranslationDataSource = remoteTranslationDataSource
    }

    private val fakeTranslations = listOf(Translation(1, "Test", "Тест", Date()),
            Translation(2, "Translation", "Переклад", Date()), Translation(3, "Word", "Слово", Date()))
}