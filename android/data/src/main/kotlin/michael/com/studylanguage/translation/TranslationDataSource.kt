package michael.com.studylanguage.translation

import io.reactivex.Maybe
import io.reactivex.Observable


/**
 * Created on 31.10.2017.
 */
interface TranslationDataSource {
    fun getAll(): Observable<Translation>

    fun getById(id:Int): Observable<Translation>

    fun getWithText(text: String): Observable<Translation>

    fun update(newvalue: Translation)

    fun removeAll()

    fun remove(id: Int)

    fun add(translation: Translation): Maybe<Translation>
}