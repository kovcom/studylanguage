package michael.com.studylanguage.translation.remote.google

import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * @author Michael
 * created on 25.09.2016.
 */
interface TranslationService {
    @POST("language/translate/v2")
    fun translateWords(@Body text: GoogleTranslationEntity, @Query("key")key:String): Observable<GoogleTranslationResponseEntity>
}