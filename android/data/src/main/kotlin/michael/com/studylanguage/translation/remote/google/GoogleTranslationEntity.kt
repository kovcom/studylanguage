package michael.com.studylanguage.translation.remote.google

/**
 * Created on 01.11.2017.
 */
data class GoogleTranslationEntity constructor(val q: String, val target: String)
