package michael.com.studylanguage.translation.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Keep it java-class because it has better SQL request highlighting.
 * <p>
 * Created on 01.11.2017.
 */
@Dao
public interface TranslationDao {
    @Query("SELECT * FROM translations")
    List<TranslationModel> all();

    @Query("DELETE FROM translations WHERE uid=:id")
    void delete(int id);

    @Query("DELETE FROM translations")
    void deleteAll();

    @Query("SELECT * FROM translations WHERE origin LIKE :origin")
    Flowable<TranslationModel> findByName(String origin);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(TranslationModel... users);

    @Query("SELECT * FROM translations WHERE uid =:id")
    Flowable<TranslationModel> loadById(int id);
}