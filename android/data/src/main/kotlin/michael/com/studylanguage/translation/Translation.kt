package michael.com.studylanguage.translation

import java.util.*

/**
 * Created on 31.10.2017.
 */
data class Translation(val id: Int, val origin: String, val translation: String?, val translatedAt: Date)