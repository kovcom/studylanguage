package michael.com.studylanguage.translation.remote

import io.reactivex.Maybe
import io.reactivex.Observable
import michael.com.studylanguage.translation.Translation
import michael.com.studylanguage.translation.TranslationDataSource

/**
 * It is mocked at the moment as there is no backend API for storing translations
 *
 * Created on 02.11.2017.
 */

class RemoteTranslationDataSource : TranslationDataSource {
    override fun add(translation: Translation): Maybe<Translation> {
        return Maybe.empty()
    }

    override fun getAll(): Observable<Translation> {
        return Observable.empty()
    }

    override fun getById(id: Int): Observable<Translation> {
        return Observable.empty()
    }

    override fun getWithText(text: String): Observable<Translation> {
        return Observable.empty()
    }

    override fun remove(id: Int) {

    }

    override fun removeAll() {

    }

    override fun update(newvalue: Translation) {

    }
}
