package michael.com.studylanguage.translation.local

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


/**
 * Created on 01.11.2017.
 */
@Entity(tableName = C.translationTN)
class TranslationModel {
    var uid: Int = 0
    @PrimaryKey
    @ColumnInfo(name = "origin")
    var origin: String = ""

    @ColumnInfo(name = "translation")
    var translation: String? = ""

    @ColumnInfo(name = "translationDate")
    var translationDate: Long? = null
}