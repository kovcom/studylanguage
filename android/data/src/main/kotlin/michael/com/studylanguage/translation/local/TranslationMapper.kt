package michael.com.studylanguage.translation.local

import michael.com.studylanguage.translation.Translation
import java.util.*

/**
 * Extension functions which helps for mappings
 * Created on 02.11.2017.
 */
fun TranslationModel.toTranslation(): Translation {
    return Translation(uid, origin, translation, Date(translationDate!!))
}

fun Translation.toModel(): TranslationModel {

    val translationModel = TranslationModel()
    translationModel.apply {
        origin = this@toModel.origin
        translation = this@toModel.translation
        translationDate = this@toModel.translatedAt.time
    }
    return translationModel
}
