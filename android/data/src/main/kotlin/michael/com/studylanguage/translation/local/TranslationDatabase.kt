package michael.com.studylanguage.translation.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase


/**
 * Created on 31.10.2017.
 */
@Database(exportSchema = false, entities = arrayOf(TranslationModel::class), version = 1)
abstract class TranslationDatabase : RoomDatabase() {
    abstract fun translationDao(): TranslationDao
}