package michael.com.studylanguage.translation.remote.google

/**
 * Created on 01.11.2017.
 */

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GoogleTranslationResponseEntity {

    @SerializedName("data")
    @Expose
    var data: Data? = null

    class Translation {

        @SerializedName("translatedText")
        @Expose
        var translatedText: String? = null
        @SerializedName("detectedSourceLanguage")
        @Expose
        var detectedSourceLanguage: String? = null
    }

    class Data {

        @SerializedName("translations")
        @Expose
        var translations: List<Translation>? = null

    }

}