package michael.com.studylanguage;

import io.reactivex.Maybe
import io.reactivex.Observable
import michael.com.studylanguage.translation.Translation
import michael.com.studylanguage.translation.TranslationDataSource
import michael.com.studylanguage.translation.TranslationDataSourceFactory
import javax.inject.Inject


open class TranslationRepository @Inject constructor(val factory: TranslationDataSourceFactory) : TranslationDataSource {
    private val localDataSource: TranslationDataSource = factory.provideLocalDataSource()
    private val remoteDataSource: TranslationDataSource = factory.provideRemoteDataSource()

    override fun add(translation: Translation): Maybe<Translation> {
        return localDataSource.add(translation).doOnComplete { remoteDataSource.add(translation) }
    }

    var isCacheDirty = true

    override fun getAll(): Observable<Translation> {
        return if (isCacheDirty) {
            remoteDataSource.getAll().doOnEach { item ->
                run {
                    if (item.value != null) {
                        localDataSource.add(item.value as Translation)
                    }
                }
            }
                    .doOnComplete {
                        isCacheDirty = false
                    }.concatWith(localDataSource.getAll())
        } else {
            localDataSource.getAll()
        }
    }

    override fun getById(id: Int): Observable<Translation> {
        return if (isCacheDirty) {
            remoteDataSource.getById(id)
        } else {
            localDataSource.getById(id)
        }
    }

    override fun getWithText(text: String): Observable<Translation> {
        return if (isCacheDirty) {
            remoteDataSource.getWithText(text)
        } else {
            localDataSource.getWithText(text)
        }
    }

    override fun update(newvalue: Translation) {
        localDataSource.update(newvalue)
        remoteDataSource.update(newvalue)
    }

    override fun removeAll() {
        localDataSource.removeAll()
        remoteDataSource.removeAll()
    }

    override fun remove(id: Int) {
        localDataSource.remove(id)
        remoteDataSource.remove(id)
    }
}
