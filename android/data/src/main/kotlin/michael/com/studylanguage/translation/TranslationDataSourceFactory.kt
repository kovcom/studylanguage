package michael.com.studylanguage.translation

/**
 * Created on 02.11.2017.
 */
interface TranslationDataSourceFactory {
    fun provideLocalDataSource(): TranslationDataSource

    fun provideRemoteDataSource(): TranslationDataSource
}