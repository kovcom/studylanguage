package michael.com.studylanguage.translation.local

import io.reactivex.Maybe
import io.reactivex.Observable
import michael.com.studylanguage.translation.Translation
import michael.com.studylanguage.translation.TranslationDataSource

/**
 * Created on 02.11.2017.
 */
class LocalTranslationDataSource constructor(private val dao: TranslationDao) : TranslationDataSource {

    override fun getAll(): Observable<Translation> {
        return Observable.defer { Observable.fromIterable(dao.all()).map { item -> item.toTranslation() } }
    }

    override fun getById(id: Int): Observable<Translation> {
        return Observable.defer { dao.loadById(id).toObservable().map { i -> i.toTranslation() } }
    }

    override fun getWithText(text: String): Observable<Translation> {
        return Observable.defer { dao.findByName(text).toObservable().map { i -> i.toTranslation() } }
    }

    override fun update(newvalue: Translation) {
        dao.insertAll(newvalue.toModel())
    }

    override fun removeAll() {
        dao.deleteAll()
    }

    override fun remove(id: Int) {
        dao.delete(id)
    }

    override fun add(translation: Translation): Maybe<Translation> {
        dao.insertAll(translation.toModel())
        return getWithText(translation.origin).firstElement()
    }
}