@echo off

rem Checking if Javac 1.8 is available
javac -version >nul 2>&1 && ( goto javacExists ) || ( goto checkAgain )
:checkAgain
"%JAVA_HOME%\bin\javac.exe" -version >nul 2>&1 && ( goto javacExists ) || ( goto noJavac )
:javacExists

rem Checking JAVA_HOME
if "%JAVA_HOME%" == "" goto noJavaHome

rem Checking if JAVA_HOME is correct
if not EXIST "%JAVA_HOME%\bin\javac.exe" goto wrongJavaHome

rem Checking if maven is installed and available on path
cmd /C mvn -version >nul 2>&1 && ( goto mavenOk ) || ( goto noMaven )
:mavenOk

rem Specify the ip address and port as the arguments on the next line.
cmd /C mvn package exec:java -Dexec.args="localhost 4321" 

if ERRORLEVEL 1 goto buildError

rem Code ends here, only error handling below.
goto end

:noJavac
echo ERROR: No Java Compiler ("javac") compiler was found. Please, install Java JDK 1.8 or later
echo See: http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
goto end

:noJavaHome
echo ERROR: No JAVA_HOME environment variable was defined. Please, define JAVA_HOME variable that points to the java home directiry
echo See: https://docs.oracle.com/cd/E19182-01/820-7851/inst_cli_jdk_javahome_t/
goto end

:wrongJavaHome
echo ERROR: JAVA_HOME variable should point to the root directory of the installed JDK.
echo See: https://docs.oracle.com/cd/E19182-01/820-7851/inst_cli_jdk_javahome_t/
goto end

:noMaven
echo ERROR: Maven is not installed or is not available in PATH
echo See: https://maven.apache.org/guides/getting-started/windows-prerequisites.html
goto end

:buildError
echo ERROR: Failed to build and start an application. See the error above.
pause.

:end
