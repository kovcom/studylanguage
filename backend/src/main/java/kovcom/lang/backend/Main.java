package kovcom.lang.backend;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.inject.Injector;

import kovcom.lang.backend.appInit.guice.*;

/**
 * Application entry point. Initializes and starts the server.
 * 
 * @author RST
 */
public class Main {

    public static void main(String[] args) {

	//
	// defining default values here.
	//
	String address = "localhost";
	int port = 4321;

	// If any command line args are provided, they will override defaults.
	if (args.length >= 2) {
	    address = args[0];
	    try {
		port = Integer.parseInt(args[1]);
	    } catch (NumberFormatException ex) {
		Logger.getLogger(Main.class.getName()).warning(
			String.format("Failed to parse '%s' as port number, default %s will be used", args[1], port));
	    }
	}

	//
	// Starting the server.
	//
	Injector injector = AppBuilder.createInjector();

	try (IServer server = injector.getInstance(IServer.class)) {

	    server.run(address, port);

	    System.out.printf("REST API Started at http://%s:%s/api%nPress ENTER to shutdown.%n", address, port);
	    System.in.read();

	} catch (Exception e) {
	    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Failed to start the server", e);
	}
    }
}
