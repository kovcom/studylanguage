package kovcom.lang.backend.dto;

/**
 * Defines a set of roles that the system is aware of.
 *
 * @author RST
 *
 */
public enum Roles {
    User(1, "User"), Admin(2, "Administrator");

    private final int index;
    private final String name;

    private Roles(int index, String name) {
        this.index = index;
        this.name = name;
    }

    public int getIndex() {
        return this.index;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public static Roles fromString(String roleString){
        for(Roles role : Roles.values()){
            if(role.name.equals(roleString))
                return role;
        }

        return null;
    }
}
