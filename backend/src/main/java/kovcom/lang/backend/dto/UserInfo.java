package kovcom.lang.backend.dto;

/**
 * An object that is being passed to <b>IUserManager</b> and contains a complete information on a new user.
 *
 * @author RST
 *
 */
public class UserInfo {

    private String login;
    private String secret;

    private String email;
    private String name;

    private String[] preferredLanguages;
    private Roles[] roles;

    public String getLogin() {
        return this.login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSecret() {
        return this.secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getPreferredLanguages() {
        return this.preferredLanguages;
    }

    public void setPreferredLanguages(String[] preferredLanguages) {
        this.preferredLanguages = preferredLanguages;
    }

    public Roles[] getRoles() {
        return this.roles;
    }

    public void setRoles(Roles[] roles) {
        this.roles = roles;
    }
}
