package kovcom.lang.backend.dto;

import java.util.List;

/**
 * An object returned from <b>ITranslationProvider</b> that contains a single translation alternative.
 * @author RST
 *
 */
public class Translation {

    private String language;
    private String translation;
    private String contextTranslation;
    private List<String> usageExamples;

    public String getLanguage() {
	return language;
    }

    public void setLanguage(String language) {
	this.language = language;
    }

    public String getTranslation() {
	return translation;
    }

    public void setTranslation(String translation) {
	this.translation = translation;
    }

    public String getContextTranslation() {
	return contextTranslation;
    }

    public void setContextTranslation(String contextTranslation) {
	this.contextTranslation = contextTranslation;
    }

    public List<String> getUsageExamples() {
	return usageExamples;
    }

    public void setUsageExamples(List<String> usageExamples) {
	this.usageExamples = usageExamples;
    }
}
