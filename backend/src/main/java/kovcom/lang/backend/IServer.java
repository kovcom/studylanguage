package kovcom.lang.backend;

import java.io.Closeable;

/**
 * A generic interface that defines a server that should be started.
 * @author RST
 *
 */
public interface IServer extends Closeable {

	/**
	 * Starts a server.
	 * @param address The address to start the server on.
	 * @param port The port to start the server on.
	 */
	void run(String address, int port);
}
