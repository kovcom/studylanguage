package kovcom.lang.backend.logging;


/**
 * An interface that handles component activities logging.
 * 
 * @author RST
 *
 */
public interface ILogger {
    /**
     * Logs an informational (trace/debug) message.
     * @param format A message or a message format ready to be passed to <b>String.format()</b>. 
     * @param params An optional collection of parameters ready to be passed to <b>String.format()</b>.
     */
    void info(String format, Object... params);
    
    /**
     * Logs an warning (not-critical error) message.
     * @param format A message or a message format ready to be passed to <b>String.format()</b>. 
     * @param params An optional collection of parameters ready to be passed to <b>String.format()</b>.
     */
    void warning(String format, Object... params);
    
    /**
     * Logs an error message.
     * @param format A message or a message format ready to be passed to <b>String.format()</b>. 
     * @param params An optional collection of parameters ready to be passed to <b>String.format()</b>.
     */
    void error(String format, Object... params);
}
