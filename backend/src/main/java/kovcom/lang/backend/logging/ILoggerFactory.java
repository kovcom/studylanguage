package kovcom.lang.backend.logging;

public interface ILoggerFactory {
    /**
     * Configures a logger based on the component provided.
     * @param component A component that will use this logger object in future.
     * @return A configured logger object.
     */
    ILogger forComponent(Object component);
    
    /**
     * Configures a logger based on the component name provided.
     * @param componentName A name of the component that will use this logger object in future.
     * @return A configured logger object.
     */
    ILogger forComponentName(String componentName);
}
