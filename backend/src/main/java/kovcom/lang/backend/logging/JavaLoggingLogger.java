package kovcom.lang.backend.logging;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Provides an <b>ILogger</b> implementation based on <b>java.util.logging</b>.
 * @author RST
 *
 */
public class JavaLoggingLogger implements ILogger, ILoggerFactory {

    private String loggerName = null;

    @Override
    public ILogger forComponent(Object component) {
	this.loggerName = component.getClass().getName();
	return this;
    }

    @Override
    public ILogger forComponentName(String componentName) {
	this.loggerName = componentName;
	return this;
    }

    @Override
    public void info(String format, Object... params) {
	log(Level.INFO, format, params);
    }

    @Override
    public void warning(String format, Object... params) {
	log(Level.WARNING, format, params);
    }

    @Override
    public void error(String format, Object... params) {
	log(Level.SEVERE, format, params);
    }

    private void log(Level level, String format, Object... params) {

	Logger logger = Logger.getLogger(this.loggerName);

	if (params == null)
	    // no parameters -- no problems.
	    logger.log(level, format);
	else
	    logger.log(level, String.format(format, params), findException(params));
    }

    /**
     * Finds exception within the provided parameters.
     * 
     * @param params
     *            The parameters to scan.
     * @return An instance of Throwable, if any. Otherwise, <b>null</b>.
     */
    private Throwable findException(Object[] params) {

	for (Object obj : params) {
	    if (obj instanceof Throwable)
		return (Throwable) obj;
	}

	return null;
    }
}
