package kovcom.lang.backend.utils;

public class StringUtils {
    private StringUtils() {
    }
    
    public static boolean isNullOrEmpty(String str){
	return str == null || str.length() == 0;
    }
    
    public static boolean isNullOrWhiteSpace(String str){
	return isNullOrEmpty(str) || str.trim().length() == 0;
    }
}
