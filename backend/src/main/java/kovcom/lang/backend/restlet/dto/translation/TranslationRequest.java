package kovcom.lang.backend.restlet.dto.translation;

import kovcom.lang.backend.utils.*;

/**
 * A root object that translation request is being deserialized into.
 *
 * @author RST
 *
 */
public class TranslationRequest {
    private String phrase;
    private String context;
    private String languageFrom;
    private String languageTo;

    public String getPhrase() {
        return this.phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public String getContext() {
        return this.context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getLanguageFrom() {
        return this.languageFrom;
    }

    public void setLanguageFrom(String languageFrom) {
        this.languageFrom = languageFrom;
    }

    public String getLanguageTo() {
        return this.languageTo;
    }

    public void setLanguageTo(String languageTo) {
        this.languageTo = languageTo;
    }

    public boolean isLanguageFromSpecified() {
        return !StringUtils.isNullOrWhiteSpace(this.languageFrom);
    }

    public boolean isLanguageToSpecified() {
        return !StringUtils.isNullOrWhiteSpace(this.languageTo);
    }
}
