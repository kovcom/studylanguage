package kovcom.lang.backend.restlet;

import com.google.inject.*;

import kovcom.lang.backend.*;

public class GuiceModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(IServer.class).to(RestletServer.class).in(Singleton.class);
    }

}
