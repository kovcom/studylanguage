package kovcom.lang.backend.restlet.api;

import kovcom.lang.backend.logging.*;
import kovcom.lang.backend.restlet.*;

public class UnknownWordsResource extends BaseResource {

    public UnknownWordsResource(ILoggerFactory logger) {
        super(logger);
    }
}
