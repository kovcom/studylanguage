package kovcom.lang.backend.restlet;

import org.restlet.Request;
import org.restlet.data.ClientInfo;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
import org.restlet.security.User;

import kovcom.lang.backend.logging.ILogger;
import kovcom.lang.backend.logging.ILoggerFactory;

/**
 * A base resource class that provides some generic methods that are useful for all REST resources.
 * 
 * @author RST
 *
 */
public abstract class BaseResource extends ServerResource {
    private static final String NO_USER_ERROR_MESSAGE = "Failed to retrieve user name on demand";
    protected static final MediaType MEDIA_TYPE = MediaType.APPLICATION_JSON;

    protected final ILogger logger;

    public BaseResource(ILoggerFactory logger) {
        this.logger = logger.forComponent(this);
    }

    /**
     * Retrieves a name of the user that invoked the current request (if any).
     * 
     * @return A <b>String</b> that identifies a name of the current user or <b>null</b>, if no user available.
     */
    protected String getUsername() {
        Request request = this.getRequest();

        if (request == null)
            throw new RuntimeException(NO_USER_ERROR_MESSAGE);

        ClientInfo clientInfo = request.getClientInfo();
        if (clientInfo == null)
            throw new RuntimeException(NO_USER_ERROR_MESSAGE);

        User user = clientInfo.getUser();
        if (user == null)
            throw new RuntimeException(NO_USER_ERROR_MESSAGE);

        String userName = user.getName();

        if (userName == null || userName.length() == 0)
            throw new RuntimeException(NO_USER_ERROR_MESSAGE);

        return userName;
    }

    /**
     * Generates a response object with a specified status.
     * 
     * @param status
     *            A response status to return.
     * @return An object that can be returned from resource.
     */
    protected Object response(Status status) {
        return response(status, "");
    }

    /**
     * Generates a response object with a specified status and response text.
     * 
     * @param status
     *            A response status to return.
     * @param responseBody
     *            An additional message to return.
     * @return An object that can be returned from resource.
     */
    protected Object response(Status status, String responseBody) {
        getResponse().setStatus(status);

        return new StringRepresentation(responseBody, MEDIA_TYPE);
    }

    /**
     * @return A response object with 200 (OK) response status and no response text.
     */
    protected Object ok() {
        return response(Status.SUCCESS_OK);
    }

    /**
     * @return A response object with 201 (CREATED) response status and no response text.
     */
    protected Object created() {
        return response(Status.SUCCESS_CREATED);
    }

    /**
     * A helper method that aborts request processing and returns an error message with the specified status code.
     * 
     * @param status
     *            A status code to return.
     * @param errorMessage
     *            An error message to return.
     * @return There's no return value as this method throws an exception unconditionally.
     */
    protected <T> T throwError(Status status, String errorMessage) {
        throw new ResourceException(status, errorMessage);
    }

    /**
     * A helper method that aborts request processing and returns a 400 (BAD REQUEST) error to the client.
     * 
     * @param errorMessage
     *            An error message to return.
     * @return There's no return value as this method throws an exception unconditionally.
     */
    protected <T> T throwBadRequest(String errorMessage) {
        return throwError(Status.CLIENT_ERROR_BAD_REQUEST, errorMessage);
    }

    /**
     * A helper method that aborts request processing and returns a 500 (SERVER ERROR) error to the client.
     * 
     * @param errorMessage
     *            An error message to return.
     * @return There's no return value as this method throws an exception unconditionally.
     */
    protected <T> T throwServerError(String errorMessage) {
        return throwError(Status.SERVER_ERROR_INTERNAL, errorMessage);
    }
}
