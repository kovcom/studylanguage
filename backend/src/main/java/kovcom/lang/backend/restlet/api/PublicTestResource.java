package kovcom.lang.backend.restlet.api;

import javax.inject.*;

import org.restlet.resource.*;

import kovcom.lang.backend.logging.*;
import kovcom.lang.backend.restlet.*;

public class PublicTestResource extends BaseResource {

    @Inject
    public PublicTestResource(ILoggerFactory logger) {
        super(logger);
    }

    @Get
    public String getResult() {
        return "HELLo World from a public test resource!";
    }
}
