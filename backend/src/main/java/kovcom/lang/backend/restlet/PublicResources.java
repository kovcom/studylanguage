package kovcom.lang.backend.restlet;

import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.ext.guice.FinderFactory;
import org.restlet.routing.Router;

import kovcom.lang.backend.restlet.api.*;

/**
 * @author RST
 * Exposes public resources that are available without any authentication/authorization.
 */
public class PublicResources extends Application {

	private final FinderFactory finderFactory;

	public PublicResources(FinderFactory finderFactory) {
		this.finderFactory = finderFactory;
	}

	@Override
	public Restlet createInboundRoot() {
		Router router = new Router(getContext());

		// new user registration resource
		router.attach("/test", this.finderFactory.finder(PublicTestResource.class));
		router.attach("/register", this.finderFactory.finder(RegisterUserResource.class));

		return router;
	}
}
