package kovcom.lang.backend.restlet.api;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

/**
 * Test resource class that just illustrates that REST api is working and
 * everything is configured properly. Can be accessed at:
 * http://server:port/api/test
 * @author RST
 */
public class TestResource extends ServerResource {

    @Get
    public String getResult() {
	return "HELLo World from a secured test resource!";
    }
}
