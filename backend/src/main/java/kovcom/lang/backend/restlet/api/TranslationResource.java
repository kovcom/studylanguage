package kovcom.lang.backend.restlet.api;

import java.util.*;
import java.util.stream.*;

import javax.inject.*;

import org.restlet.resource.*;

import kovcom.lang.backend.dataAccess.*;
import kovcom.lang.backend.dto.*;
import kovcom.lang.backend.logging.*;
import kovcom.lang.backend.restlet.*;
import kovcom.lang.backend.restlet.dto.translation.*;
import kovcom.lang.backend.utils.*;

/**
 * A translation resource that handles translation requests.
 *
 * @author RST
 *
 */
public class TranslationResource extends BaseResource {

    private final ITranslationProvider translator;
    private final IUserManager userManager;

    @Inject
    public TranslationResource(ITranslationProvider translator, IUserManager userManager, ILoggerFactory logger) {
        super(logger);

        this.translator = translator;
        this.userManager = userManager;
    }

    /**
     * A method that handles translation requests.
     *
     * @param request
     *            An object that contains translation request data.
     * @return A translation request response or an error status code.
     */
    @Post("json")
    public TranslationResponse getTranslation(TranslationRequest request) {
        try {

            validateRequest(request);

            return translate(request);

        } catch (IllegalArgumentException ex) {
            return throwBadRequest(ex.getMessage());
        } catch (Exception ex) {
            this.logger.warning("Translation of %s resulted in an exception: %s", request, ex.getMessage(), ex);
            return throwServerError("Failed to process your request.");
        }
    }

    /**
     * Validates if the provided request object is valid.
     *
     * @param request
     *            An object to verify.
     */
    private void validateRequest(TranslationRequest request) {
        if (request == null)
            throwBadRequest("The request cannot be null");

        if (request.getPhrase() == null || request.getPhrase().trim().length() == 0)
            throwBadRequest("The phrase to translate cannot be empty.");
    }

    /**
     * Performs the translation and forms a response object.
     *
     * @param request
     *            A translation request parameters.
     * @return An object that contains translation response.
     */
    private TranslationResponse translate(TranslationRequest request) {
        // preparing translation parameters.
        String languageFrom = request.isLanguageFromSpecified() ? request.getLanguageFrom() : null;
        String[] languagesTo = request.isLanguageToSpecified() ? new String[] { request.getLanguageTo() }
                                                               : this.userManager.getPreferredLanguages(this.getUsername());

        String context = StringUtils.isNullOrWhiteSpace(request.getContext()) ? request.getPhrase() : request.getContext();

        // translating.
        List<Translation> result = this.translator.translate(request.getPhrase(), context, languageFrom, languagesTo);

        // preparing response object.
        TranslationResponse response = new TranslationResponse();
        response.setTranslatedPhrase(request.getPhrase());
        response.setContext(request.getContext());

        // forming result.
        if (result != null && result.size() > 0) {

            // transforming translations.
            response.setTranslations(result.stream()
                                           .collect(Collectors.groupingBy(Translation::getLanguage))
                                           .entrySet().stream()
                                           .map(entry -> new LanguageTranslation(entry.getKey(),
                                                                                 createTranslationInfos(entry.getValue())))
                                           .toArray(size -> new LanguageTranslation[size]));
        }

        return response;
    }

    /**
     * A helper method that transforms a collection of translations into an array of TranslationInfo objects.
     *
     * @param translations
     *            A translations to process.
     * @return An array of TranslationInfo object that contains information from <b>translations</b> collection.
     */
    private TranslationInfo[] createTranslationInfos(List<Translation> translations) {
        return translations.stream()
                           .map(elem -> new TranslationInfo(elem.getTranslation(),
                                                            elem.getContextTranslation(),
                                                            elem.getUsageExamples()
                                                                .stream()
                                                                .toArray(size -> new String[size])))
                           .toArray(size -> new TranslationInfo[size]);
    }
}
