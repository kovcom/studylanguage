package kovcom.lang.backend.restlet.dto.translation;

/**
 * A part of <b>LanguageTranslation</b> that holds a single translation alternative.
 *
 * @author RST
 *
 */
public class TranslationInfo {
    private String phrase;
    private String context;
    private String[] examples;

    public TranslationInfo() {
    }

    public TranslationInfo(String word, String context, String[] examples) {
        this.phrase = word;
        this.context = context;
        this.examples = examples;
    }

    public String getPhrase() {
        return this.phrase;
    }

    public void setPhrase(String word) {
        this.phrase = word;
    }

    public String getContext() {
        return this.context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String[] getExamples() {
        return this.examples;
    }

    public void setExamples(String[] examples) {
        this.examples = examples;
    }
}
