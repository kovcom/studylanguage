package kovcom.lang.backend.restlet.dto.translation;

/**
 * A part of <b>TranslationResponse</b> that holds translations on a single language.
 *
 * @author RST
 *
 */
public class LanguageTranslation {
    private String language;
    private TranslationInfo[] translations;

    public LanguageTranslation() {
    }

    public LanguageTranslation(String language, TranslationInfo[] translations) {
        this.language = language;
        this.translations = translations;
    }

    public String getLanguage() {
        return this.language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public TranslationInfo[] getTranslations() {
        return this.translations;
    }

    public void setTranslations(TranslationInfo[] translations) {
        this.translations = translations;
    }
}
