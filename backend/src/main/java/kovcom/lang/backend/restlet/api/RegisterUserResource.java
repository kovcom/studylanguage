package kovcom.lang.backend.restlet.api;

import javax.inject.Inject;

import org.restlet.resource.Get;
import org.restlet.resource.Post;

import kovcom.lang.backend.dataAccess.IUserManager;
import kovcom.lang.backend.dto.*;
import kovcom.lang.backend.logging.ILoggerFactory;
import kovcom.lang.backend.restlet.BaseResource;
import kovcom.lang.backend.restlet.dto.users.NewUserInfo;

/**
 * A resource that registers users in a provided user manager.
 * 
 * @author RST
 */
public class RegisterUserResource extends BaseResource {
    private static final Roles[] DEFAULT_ROLES = new Roles[] { Roles.User };

    private final IUserManager userManager;

    @Inject
    public RegisterUserResource(IUserManager userManager, ILoggerFactory logger) {
        super(logger);
        this.userManager = userManager;
    }

    /**
     * TODO: remove this method. A testing method that allows verifying that the Register User resource is available.
     * 
     * @return A string that identifies this resource.
     */
    @Get
    public String test() {
        return String.format("Register user resource is here.");
    }

    /**
     * Processes user registration requests.
     * 
     * @param newUser
     *            An object that contains new user registration information.
     * @return A status code that identifies if the registration was successfull or not.
     */
    @Post
    public Object register(NewUserInfo newUser) {

        validate(newUser);

        try {
            // creating a user info object.
            UserInfo userInfo = newUser.toUserInfo();

            // granting default user roles.
            userInfo.setRoles(DEFAULT_ROLES);

            // registering a user.
            this.userManager.register(userInfo);

            return created();

        } catch (IllegalArgumentException ex) {
            return throwBadRequest(ex.getMessage());
        } catch (Exception ex) {
            this.logger.warning("Failed to register '%s': %s", newUser.getLogin(), ex.getMessage(), ex);
            return throwServerError("Failed to register a user.");
        }
    }

    /**
     * Validates if the provided <b>userInfo</b> object is valid.
     * 
     * @param userInfo
     *            An object to verify.
     */
    private void validate(NewUserInfo userInfo) {
        if (userInfo == null)
            throwBadRequest("The user information has to be specified.");

        if (userInfo.getLogin() == null || userInfo.getLogin().trim().length() == 0)
            throwBadRequest("The user's login has to be specified.");

        if (userInfo.getPassword() == null || userInfo.getPassword().length() == 0)
            throwBadRequest("The user's password has to be specified.");
    }
}
