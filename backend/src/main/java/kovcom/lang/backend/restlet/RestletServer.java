package kovcom.lang.backend.restlet;

import javax.inject.Inject;

import org.restlet.Application;
import org.restlet.Component;
import org.restlet.Restlet;
import org.restlet.data.Protocol;
import org.restlet.ext.crypto.DigestAuthenticator;
import org.restlet.ext.guice.FinderFactory;
import org.restlet.routing.Router;
import org.restlet.security.ChallengeAuthenticator;

import kovcom.lang.backend.IServer;
import kovcom.lang.backend.dataAccess.IUserManager;
import kovcom.lang.backend.logging.ILogger;
import kovcom.lang.backend.logging.ILoggerFactory;
import kovcom.lang.backend.restlet.api.TestResource;
import kovcom.lang.backend.restlet.api.TranslationResource;
import kovcom.lang.backend.restlet.security.Authorizer;

/**
 * RESTLET implementation of API Server. Contains basic server configuration and
 * resource mapping.
 * 
 * @author RST
 */
public class RestletServer extends Application implements IServer {

    /**
     * The name of the domain ("realm") that we want users to authenticate in.
     * Possibly, we can have a site name here.
     */
    private static final String AUTH_REALM = "KovKom";
    /**
     * Super-secret password needed for the DIGEST authentication on the server.
     * TODO: we'd better configure this somewhere...
     */
    private static final String SERVER_SECRET_KEY = "6ygBy8sVrSSpKJZmSYVg";

    private final ILogger logger;

    private final Component restletComponent;
    private final FinderFactory finderFactory;
    private final IUserManager userManager;

    @Inject
    public RestletServer(FinderFactory finderFactory, IUserManager userManager, ILoggerFactory logger) {
	this.logger = logger.forComponent(this);

	this.finderFactory = finderFactory;
	this.restletComponent = new Component();
	this.userManager = userManager;
    }

    @Override
    public void close() {
	try {
	    this.restletComponent.stop();
	} catch (Exception e) {
	    this.logger.warning("Failed to shutdown the API Server", e);
	}
    }

    @Override
    public void run(String address, int port) {

	// creating a server. TODO: switch to HTTPS here at some point.
	this.restletComponent.getServers().add(Protocol.HTTP, address, port);

	// attaching application that exposes public API.
	this.restletComponent.getDefaultHost().attach("/public", new PublicResources(this.finderFactory));

	// attaching main application (secured).
	this.restletComponent.getDefaultHost().attach("/api", this);

	// TODO: To disable/manage server console logging, uncomment/rewrite the
	// following line.
	// component.setLogService(new NullLogService());

	// starting the server.
	try {
	    this.restletComponent.start();
	} catch (Exception e) {
	    throw new RuntimeException("Failed to start the API Server", e);
	}
    }

    @Override
    public Restlet createInboundRoot() {
	//
	// configuring request processing tool-chain.
	// Outer methods are supposed to process request sooner.
	// i.e., authenticate, authorize, select resource, process.
	//
	return enableAuthentication(registerResources(new Router(getContext())));
    }

    /**
     * Registers all resources that belong to this application.
     * 
     * @param router
     *            The router resources should be attached to.
     */
    private Router registerResources(Router router) {

	// Adding test resource. TODO: Remove it when initial testing is done.
	router.attach("/test", this.finderFactory.finder(TestResource.class));

	// Adding translate resource that handles translation requests.
	router.attach("/translate", this.finderFactory.finder(TranslationResource.class));

	return router;
    }

    /**
     * Configures authentication that wraps the provided router.
     * 
     * @param router
     *            The router that should be executed for authenticated users.
     * @return Secured router instance.
     */
    private ChallengeAuthenticator enableAuthentication(Router router) {

	// Configuring authentication method here.
	DigestAuthenticator guard = new DigestAuthenticator(getContext(), AUTH_REALM, SERVER_SECRET_KEY);

	// Creating the authorizer that takes care of authentication and
	// authorization.
	Authorizer authorizer = new Authorizer(this, this.userManager);

	// registering it for authorization and authentication.
	guard.setWrappedVerifier(authorizer);
	guard.setEnroler(authorizer);

	// registering router with all resources to execute next.
	guard.setNext(router);

	return guard;
    }
}
