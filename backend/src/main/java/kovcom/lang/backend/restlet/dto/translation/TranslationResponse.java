package kovcom.lang.backend.restlet.dto.translation;

/**
 * A root object that translation response is being formed and serialized from.
 *
 * @author RST
 *
 */
public class TranslationResponse {
    private String translatedPhrase;
    private String context;
    private LanguageTranslation[] translations;

    public LanguageTranslation[] getTranslations() {
        return this.translations;
    }

    public void setTranslations(LanguageTranslation[] translations) {
        this.translations = translations;
    }

    public String getTranslatedPhrase() {
        return this.translatedPhrase;
    }

    public void setTranslatedPhrase(String translatedWord) {
        this.translatedPhrase = translatedWord;
    }

    public String getContext() {
        return this.context;
    }

    public void setContext(String wordContext) {
        this.context = wordContext;
    }
}
