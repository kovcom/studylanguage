package kovcom.lang.backend.restlet.dto.users;

import kovcom.lang.backend.dto.*;

/**
 * A root new user request object that new user request is being deserialized into.
 *
 * @author RST
 *
 */
public class NewUserInfo {

    private String login;
    private String password;

    private String email;
    private String name;

    private String[] preferredLanguages;

    public String getLogin() {
        return this.login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getPreferredLanguages() {
        return this.preferredLanguages;
    }

    public void setPreferredLanguages(String[] preferredLanguages) {
        this.preferredLanguages = preferredLanguages;
    }

    public UserInfo toUserInfo() {
        UserInfo userInfo = new UserInfo();

        userInfo.setLogin(this.login);
        userInfo.setSecret(this.password);
        userInfo.setName(this.name);
        userInfo.setEmail(this.email);
        userInfo.setPreferredLanguages(this.preferredLanguages);

        return userInfo;
    }
}
