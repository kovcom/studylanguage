package kovcom.lang.backend.restlet.security;

import java.util.*;
import java.util.stream.*;

import org.restlet.*;
import org.restlet.data.*;
import org.restlet.security.*;

import kovcom.lang.backend.dataAccess.*;
import kovcom.lang.backend.dto.*;

/**
 * Manages users' <b>authentication</b>: detecting if login/password is correct; <b>authorization</b>: assigning roles for users;
 *
 * @author RST
 *
 */
public class Authorizer extends LocalVerifier implements Enroler {

    private final Application parentApplication;
    private final IUserManager userManager;

    public Authorizer(Application parentApplication, IUserManager userManager) {
        this.parentApplication = parentApplication;
        this.userManager = userManager;
    }

    @Override
    public char[] getLocalSecret(String login) {
        // Here we verify if we have the user specified by login and return
        // the password we expect.

        String secret = this.userManager.getSecret(login);
        return secret != null ? secret.toCharArray() : null;
    }

    @Override
    public void enrole(ClientInfo clientInfo) {
        // Here we enrich client info with the roles that this client has.

        if (clientInfo == null || clientInfo.getUser() == null)
            return;

        String login = clientInfo.getUser().getName();

        Roles[] roles = this.userManager.getRoles(login);

        if (roles != null) {
            clientInfo.setRoles(Arrays.stream(roles).map(role -> Role.get(this.parentApplication, role.toString()))
                                      .collect(Collectors.toList()));
        }
    }
}
