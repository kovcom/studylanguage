package kovcom.lang.backend.appInit.guice;

import com.google.inject.*;

import kovcom.lang.backend.logging.*;

/**
 * Defines application IoC configuration.
 *
 * @author RST
 */
public class AppBuilder extends AbstractModule {
    /**
     * Constructs an injector that assists building application.
     *
     * @return An instance of <b>Injector</b> that may be used to instantiate application modules.
     */
    public static Injector createInjector() {
        return Guice.createInjector(ModuleLocator.getModules());
    }

    @Override
    protected void configure() {

        // following this approach:
        // bind(TransactionLog.class).to(DatabaseTransactionLog.class);

        bind(ILoggerFactory.class).to(JavaLoggingLogger.class);

    }
}
