package kovcom.lang.backend.appInit.guice;

import java.lang.reflect.*;
import java.util.*;
import java.util.stream.*;

import com.google.inject.*;

import kovcom.lang.backend.utils.*;

class ModuleLocator {
    private static final String MODULES_CONFIG = "guice-di";
    private static final Class<?>[][] ALLOWED_CONSTRUCTOR_ARGS = new Class<?>[][] { new Class<?>[0], new Class<?>[] { Module[].class } };
    private static final Object[][] CONSTRUCTOR_ARGS = new Object[][] { new Object[0], new Object[] { new Module[0] } };

    static Iterable<AbstractModule> getModules() {
        String modulesString = System.getProperty(MODULES_CONFIG);

        List<AbstractModule> result = new ArrayList<>();

        // adding core configuration.
        result.add(new AppBuilder());

        // loading additional configuration.
        if (!StringUtils.isNullOrWhiteSpace(modulesString)) {
            result.addAll(Arrays.stream(modulesString.split("[,;]"))
                                .map(moduleName -> moduleName.trim())
                                .map(moduleName -> createModuleInstance(moduleName))
                                .collect(Collectors.toList()));
        } else
            System.out.println("WARNING: no additional modules detected, application may not function properly.");
        return result;
    }

    private static AbstractModule createModuleInstance(String moduleName) {

        try {
            Class<?> cls = Class.forName(moduleName);

            for (int i = 0; i < ALLOWED_CONSTRUCTOR_ARGS.length; i++) {
                try {
                    Constructor<?> constructor = cls.getConstructor(ALLOWED_CONSTRUCTOR_ARGS[i]);
                    return (AbstractModule) constructor.newInstance(CONSTRUCTOR_ARGS[i]);
                } catch (NoSuchMethodException
                         | InstantiationException
                         | IllegalAccessException
                         | IllegalArgumentException
                         | InvocationTargetException ex) {
                    System.out.println("EX: " + ex.getMessage());
                }
            }

            throw new RuntimeException("No supported constructor detected for class " + cls.getName());

        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Failed to find class '" + moduleName + "'", e);
        }
    }
}
