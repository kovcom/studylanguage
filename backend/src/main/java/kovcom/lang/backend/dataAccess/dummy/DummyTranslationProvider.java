package kovcom.lang.backend.dataAccess.dummy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import kovcom.lang.backend.dataAccess.ITranslationProvider;
import kovcom.lang.backend.dto.Translation;

/**
 * A dummy class that provides some random-based translations.
 * @author RST
 *
 */
public class DummyTranslationProvider implements ITranslationProvider {

    private final Random rnd = new Random(UUID.randomUUID().hashCode());

    @Override
    public List<Translation> translate(String phrase, String context, String languageFrom, String[] languagesTo) {
	List<Translation> result = new ArrayList<Translation>();

	if (languagesTo != null && languagesTo.length > 0) {
	    for (String language : languagesTo) {
		int count = rnd.nextInt(5);
		for (int i = 0; i < count; ++i) {
		    String translatedPhrase = String.format("%s%d", phrase, i + 1);
		    Translation translation = new Translation();
		    translation.setLanguage(language);
		    translation.setTranslation(translatedPhrase);
		    if (context != null && context.length() > 0) {
			translation.setContextTranslation(context.replace(phrase, translatedPhrase));
			translation.setUsageExamples(Arrays.asList(translation.getContextTranslation()));
		    }

		    result.add(translation);
		}
	    }
	}

	return result;
    }

}
