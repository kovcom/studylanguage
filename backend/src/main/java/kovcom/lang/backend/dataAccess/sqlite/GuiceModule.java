package kovcom.lang.backend.dataAccess.sqlite;

import com.google.inject.*;

import kovcom.lang.backend.dataAccess.*;
import kovcom.lang.backend.dataAccess.dummy.*;

public class GuiceModule extends AbstractModule {

    @Override
    protected void configure() {
        // TODO Auto-generated method stub

        bind(IDBManager.class).to(SQLiteDBManager.class).in(Singleton.class);

        bind(IUserManager.class).to(SQLiteUserManager.class).in(Singleton.class);

        bind(ITranslationProvider.class).to(DummyTranslationProvider.class).in(Singleton.class);
        /*
         * bind(IUserManager.class).to(DummyUserManager.class).in(Singleton.class);
         * bind(ITranslationProvider.class).to(DummyTranslationProvider.class).in(Singleton.class);
         *
         */
    }
}
