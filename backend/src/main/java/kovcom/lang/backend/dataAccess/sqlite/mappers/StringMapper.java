package kovcom.lang.backend.dataAccess.sqlite.mappers;

import java.sql.*;

import kovcom.lang.backend.dataAccess.sqlite.IDBManager.*;

public class StringMapper implements IMapper<String> {

    private final int columnIndex;

    public StringMapper(){
        this(1);
    }

    public StringMapper(int columnIndex) {
        this.columnIndex = columnIndex;
    }

    @Override
    public String map(ResultSet result) throws SQLException {
        return result.getString(this.columnIndex);
    }
}
