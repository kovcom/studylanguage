package kovcom.lang.backend.dataAccess.dummy;

import java.util.concurrent.ConcurrentHashMap;

import kovcom.lang.backend.dataAccess.IUserManager;
import kovcom.lang.backend.dto.*;

/**
 * A dummy class that provides a memory-based user manager.
 * @author RST
 *
 */
public class DummyUserManager implements IUserManager {

    private ConcurrentHashMap<String, UserInfo> users = new ConcurrentHashMap<>();

    @Override
    public void register(UserInfo user) {

	if (user == null || user.getLogin() == null || user.getLogin().trim().length() == 0)
	    throw new IllegalArgumentException("The user's login cannot be empty");

	if (users.putIfAbsent(user.getLogin(), user) != null)
	    throw new IllegalArgumentException("The specified user already exists");
    }

    @Override
    public String getSecret(String login) {

	if (login != null) {
	    UserInfo userInfo = this.users.get(login);

	    if (userInfo != null)
		return userInfo.getSecret();
	}

	return null;
    }

    @Override
    public Roles[] getRoles(String login) {
	if (login != null) {
	    UserInfo userInfo = this.users.get(login);

	    if (userInfo != null)
		return userInfo.getRoles();
	}

	return null;
    }

    @Override
    public String[] getPreferredLanguages(String login) {
	if (login != null) {
	    UserInfo userInfo = this.users.get(login);

	    if (userInfo != null)
		return userInfo.getPreferredLanguages();
	}

	return null;
    }
}
