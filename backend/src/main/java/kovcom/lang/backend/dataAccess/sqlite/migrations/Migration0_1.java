package kovcom.lang.backend.dataAccess.sqlite.migrations;

import java.sql.*;
import java.util.*;
import java.util.stream.*;

import kovcom.lang.backend.dataAccess.sqlite.*;
import kovcom.lang.backend.dto.*;
import kovcom.lang.backend.logging.*;

public class Migration0_1 extends AbstractSchemaMigration {

    public Migration0_1(ILoggerFactory logger) {
        super(null, logger);
    }

    @Override
    public String getVersion() {
        return "0.1";
    }

    @Override
    protected void performMigration(IDBManager dbManager) {
        throw new RuntimeException("This migration has no migration rules configured, as this is the original database schema builder.");
    }

    @Override
    protected Object createSchema(Statement statement) throws SQLException {

        // creating users table. Login is unique, thus it is promised to be indexed (https://www.tutorialspoint.com/sqlite/sqlite_indexes.htm).
        statement.execute("CREATE TABLE Users(Id INTEGER PRIMARY KEY ASC, Login TEXT NOT NULL UNIQUE, Password TEXT, Name TEXT, Email TEXT)");

        // creating roles table.
        statement.execute("CREATE TABLE Roles(Id INTEGER PRIMARY KEY ASC, Name Text)");

        // creating users' roles.
        statement.execute("CREATE TABLE UserRoles(Id INTEGER PRIMARY KEY ASC, "
                          + "UserId INTEGER, "
                          + "RoleId INTEGER, "
                          + "FOREIGN KEY(UserId) REFERENCES Users(Id), "
                          + "FOREIGN KEY(RoleId) REFERENCES Roles(Id))");

        // creating users' preferred languages.
        statement.execute("CREATE TABLE UserLanguages(Id INTEGER PRIMARY KEY ASC, "
                          + "UserId INTEGER, "
                          + "Language TEXT, "
                          + "FOREIGN KEY(UserId) REFERENCES Users(Id))");

        // inserting default data.
        insertRoles(statement);
        insertAdmin(statement);

        // creating common stuff.
        return super.createSchema(statement);
    }

    private void insertRoles(Statement statement) throws SQLException {
        // mapping roles to the INSERT syntax.
        String roles = String.join(",", Arrays.stream(Roles.values())
                                              .map(role -> String.format("(%d, '%s')", role.getIndex(), role))
                                              .collect(Collectors.<String> toList()));

        statement.executeUpdate("INSERT INTO Roles VALUES " + roles + ";");
    }

    private void insertAdmin(Statement statement) throws SQLException {
        String adminLogin = "Administrator";
        String adminPassword = generatePassword(10);

        this.log.info("Creating a root admin with login ='%s' and password='%s'", adminLogin, adminPassword);

        statement.executeUpdate(String.format("INSERT INTO Users VALUES (1, '%s', '%s', '%s', '%s');", adminLogin, adminPassword,
                                              "System Administrator", "admin@admin.admin"));

        statement.executeUpdate("INSERT INTO UserRoles VALUES (1, 1, 2);");
    }

    private String generatePassword(int length) {

        // generating a random byte array.
        byte[] bytes = new byte[length * 2];
        new Random(UUID.randomUUID().hashCode()).nextBytes(bytes);

        // encoding those random array into a string of the specific length. It has to be long enough as we asked for twice as much bytes.
        return new String(Base64.getEncoder().encode(bytes)).substring(0, length);
    }
}
