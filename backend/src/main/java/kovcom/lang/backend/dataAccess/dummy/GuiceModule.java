package kovcom.lang.backend.dataAccess.dummy;

import com.google.inject.*;

import kovcom.lang.backend.dataAccess.*;

public class GuiceModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(IUserManager.class).to(DummyUserManager.class).in(Singleton.class);
        bind(ITranslationProvider.class).to(DummyTranslationProvider.class).in(Singleton.class);
    }
}
