package kovcom.lang.backend.dataAccess;

import kovcom.lang.backend.dto.*;

/**
 * Defines methods that allow managing available users.
 * 
 * @author RST
 *
 */
public interface IUserManager {

    /**
     * Registers a provided user in the user storage.
     * 
     * @param user
     *            The user to register.
     */
    void register(UserInfo user);

    /**
     * Retrieves the password for the specified user.
     * 
     * @param login
     *            The login of the user to retrieve a password for.
     * @return A password of the specified user or <b>null</b> if the specified
     *         user was not found.
     */
    String getSecret(String login);

    /**
     * Retrieves the roles associated with the specified user.
     * 
     * @param login
     *            The login of the user to retrieve roles for.
     * @return A collection of roles granted to this user or <b>null</b> if the
     *         user was not found.
     */
    Roles[] getRoles(String login);

    /**
     * Retrieves the preferred languages associated with the current user.
     * 
     * @param login
     *            The login of the user to retrieve languages for.
     * @return A collection of language codes associated with this user or
     *         <b>null</b> if the user was not found or has no languages
     *         associated.
     */
    String[] getPreferredLanguages(String login);
}
