package kovcom.lang.backend.dataAccess.sqlite.migrations;

import kovcom.lang.backend.dataAccess.sqlite.*;

public interface ISchemaMigration {

    String getName();

    String getVersion();

    void migrate(IDBManager dbManager, String fromVersion);

    void create(IDBManager dbManager);

    String readCurrentVersion(IDBManager dbManager);
}
