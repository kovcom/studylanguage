package kovcom.lang.backend.dataAccess.sqlite;

import java.sql.*;
import java.util.*;
import java.util.stream.*;

import javax.inject.*;

import kovcom.lang.backend.dataAccess.*;
import kovcom.lang.backend.dataAccess.sqlite.mappers.*;
import kovcom.lang.backend.dto.*;

class SQLiteUserManager implements IUserManager {

    private static final String SECRET_STATEMENT = "SELECT Password FROM Users WHERE Login = ?";
    private static final String ROLES_STATEMENT = "SELECT role.Name FROM Users AS usr "
                                                  + "INNER JOIN UserRoles AS u2r ON usr.Id = u2r.UserId "
                                                  + "INNER JOIN Roles AS role ON role.Id = u2r.RoleId "
                                                  + "WHERE usr.Login = ?";
    private static final String LANGUAGES_STATEMENT = "SELECT Language FROM UserLanguages AS lng "
                                                      + "INNER JOIN Users AS usr ON usr.Id = lng.UserId "
                                                      + "WHERE usr.Login = ?";

    private final IDBManager dbManager;

    @Inject
    public SQLiteUserManager(IDBManager dbManager) {
        this.dbManager = dbManager;
    }

    @Override
    public void register(UserInfo user) {
        this.dbManager.execute(statement -> registerUser(statement, user));
    }

    @Override
    public String getSecret(String login) {
        return this.dbManager.querySingleOrDefault(SECRET_STATEMENT, new StringMapper(1), login);
    }

    @Override
    public Roles[] getRoles(String login) {
        return this.dbManager.query(ROLES_STATEMENT, new StringMapper(1), login)
                             .stream().map(role -> Roles.fromString(role))
                             .toArray(size -> new Roles[size]);
    }

    @Override
    public String[] getPreferredLanguages(String login) {
        List<String> languages = this.dbManager.query(LANGUAGES_STATEMENT, new StringMapper(1), login);
        return languages.toArray(new String[languages.size()]);
    }

    private Object registerUser(Statement statement, UserInfo user) throws SQLException {
        // inserting user info.
        statement.execute(String.format("INSERT INTO Users VALUES (null, '%s', '%s','%s','%s')", user.getLogin(), user.getSecret(),
                                        user.getName(), user.getEmail()));

        // apparently, SQLite works quite strange over here and instead of adding RETURN_GENERATED_KEYS to the statement above, we have to do the
        // following hack:
        int userIndex = statement.getGeneratedKeys().getInt("last_insert_rowid()");

        // inserting roles.
        String roles = String.join(",", Arrays.stream(user.getRoles())
                                              .map(role -> String.format("(null, '%d', '%d')", userIndex, role.getIndex()))
                                              .collect(Collectors.toList()));
        statement.executeUpdate(String.format("INSERT INTO UserRoles VALUES %s", roles));

        // inserting languages.
        String languages = String.join(",", Arrays.stream(user.getPreferredLanguages())
                                                  .map(language -> String.format("(null, '%d', '%s')", userIndex, language))
                                                  .collect(Collectors.toList()));
        statement.executeUpdate(String.format("INSERT INTO UserLanguages VALUES %s", languages));

        return null;
    }
}
