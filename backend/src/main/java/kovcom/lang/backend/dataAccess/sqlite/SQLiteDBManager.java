package kovcom.lang.backend.dataAccess.sqlite;

import java.io.*;
import java.sql.*;
import java.util.*;

import javax.inject.*;

import kovcom.lang.backend.dataAccess.sqlite.migrations.*;
import kovcom.lang.backend.logging.*;
import kovcom.lang.backend.utils.*;

class SQLiteDBManager implements Closeable, IDBManager {
    private static final String DB_LOCATION_VARIABLE = "sqlite-db-path";

    private final ILogger log;
    private final Connection connection;

    @Inject
    public SQLiteDBManager(ILoggerFactory logger) {

        this.log = logger.forComponent(this);
        String dbPath = getDatabasePath();

        ensureJDBCLoaded();

        this.connection = createConnection(dbPath);

        verifyDatabaseSchema(logger);
    }

    @Override
    public void close() {
        if (this.connection != null) {
            try {
                this.connection.close();
            } catch (SQLException e) {
            }
        }
    }

    @Override
    public <T> T execute(ICommand<T> command) {
        try {
            try (Statement statement = this.connection.createStatement()) {
                return command.run(statement);
            }
        } catch (Exception ex) {
            throw new RuntimeException("Command execution failed", ex);
        }
    }

    @Override
    public <T> List<T> query(String query, IMapper<T> mapper, Object... params) {
        try {
            try (PreparedStatement statement = this.connection.prepareStatement(query)) {
                setParameters(statement, params);

                try (ResultSet resultSet = statement.executeQuery()) {
                    List<T> results = new ArrayList<>();
                    while (resultSet.next()) {
                        results.add(mapper.map(resultSet));
                    }

                    return results;
                }
            }
        } catch (Exception ex) {
            throw new RuntimeException("Query '" + query + "' execution failed", ex);
        }
    }

    @Override
    public <T> T querySingle(String query, IMapper<T> mapper, Object... params) {
        List<T> results = query(query, mapper, params);

        if (results.size() != 1)
            throw new RuntimeException("Failed to query a single element. Got " + results.size() + " results instead of 1.");

        return results.get(0);
    }

    @Override
    public <T> T querySingleOrDefault(String query, IMapper<T> mapper, Object... params) {
        List<T> results = query(query, mapper, params);

        return results.size() > 0 ? results.get(0) : null;
    }

    private String getDatabasePath() {
        String result = System.getProperty(DB_LOCATION_VARIABLE);

        if (StringUtils.isNullOrWhiteSpace(result)) {
            result = "main.db";
            this.log.warning("No System Property '%s' was found. Using default dabase '%s'", DB_LOCATION_VARIABLE, result);
        } else {
            this.log.info("Using pre-configured database '%s'", result);
        }

        return result;
    }

    private void ensureJDBCLoaded() {
        try {
            this.getClass().getClassLoader().loadClass("org.sqlite.JDBC");
            this.log.info("SQLite JDBC class was found.");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Failed to load SQLite JDBC class.", e);
        }
    }

    private Connection createConnection(String dbPath) {
        try {
            return java.sql.DriverManager.getConnection(String.format("jdbc:sqlite:%s", dbPath));
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to connect to the database \"" + dbPath + "\".");
        }
    }

    private void verifyDatabaseSchema(ILoggerFactory logger) {
        ISchemaMigration currentSchema = new Migration0_1(logger);

        String currentVersion = currentSchema.readCurrentVersion(this);

        if (StringUtils.isNullOrWhiteSpace(currentVersion))
            currentSchema.create(this);
        else
            currentSchema.migrate(this, currentVersion);
    }

    private void setParameters(PreparedStatement statement, Object[] params) throws SQLException {
        if (params == null || params.length == 0)
            return;

        for (int i = 0; i < params.length; i++) {
            setParameter(statement, i + 1, params[i]);
        }
    }

    private void setParameter(PreparedStatement statement, int index, Object value) throws SQLException {
        if (value == null)
            // TODO: for now we support only string NULLs. We'd better change that in future.
            statement.setNull(index, Types.NVARCHAR);
        else if (value instanceof Integer)
            statement.setInt(index, (Integer) value);
        else if (value instanceof String)
            statement.setString(index, (String) value);
        else
            throw new RuntimeException("Not supported parameter type " + value.getClass().getName());
    }
}
