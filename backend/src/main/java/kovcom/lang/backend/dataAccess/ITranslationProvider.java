package kovcom.lang.backend.dataAccess;

import java.util.List;

import kovcom.lang.backend.dto.Translation;


/**
 * Defines an interface that provides phrase translation.
 * @author RST
 *
 */
public interface ITranslationProvider {
    
    /**
     * Translates a phrase within a context to the specified set of languages.
     * @param phrase A phrase to translate.
     * @param context A context of the phrase to use.
     * @param languageFrom An optional parameter that defines a language that initial phrase is on.
     * @param languagesTo A set of languages that the initial phrase should be translated to.
     * @return A collection of <b>Translation</b> objects that define alternative translations.
     */
    List<Translation> translate(String phrase, String context, String languageFrom, String[] languagesTo);
}
