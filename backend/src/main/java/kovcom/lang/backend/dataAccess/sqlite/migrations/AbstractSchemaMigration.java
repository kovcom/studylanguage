package kovcom.lang.backend.dataAccess.sqlite.migrations;

import java.sql.*;
import java.util.*;

import kovcom.lang.backend.dataAccess.sqlite.*;
import kovcom.lang.backend.logging.*;

public abstract class AbstractSchemaMigration implements ISchemaMigration {
    private static final String INFO_TABLE = "DatabaseInfo";

    protected final ILogger log;
    protected final ISchemaMigration previousVersion;

    protected AbstractSchemaMigration(ISchemaMigration previousVersion, ILoggerFactory logger) {
        this.log = logger.forComponent(this);
        this.previousVersion = previousVersion;
    }

    @Override
    public String getName() {
        if (this.previousVersion != null)
            return String.format("SQLite %s->%s migrator", this.previousVersion.getVersion(), getVersion());
        else
            return String.format("SQLite %s builder", getVersion());
    }

    @Override
    public void migrate(IDBManager dbManager, String fromVersion) {

        // checking if this is already the current version.
        if (getVersion().equals(fromVersion))
            return;

        // checking if the current migrator can migrate from this database.
        if (!canMigrateFrom(fromVersion)) {
            // if we can not migrate from the current version, we should try the older migrator.
            if (this.previousVersion != null) {
                this.log.info("%s can not migrate to %s, trying older migrator", getName(), fromVersion);
                this.previousVersion.migrate(dbManager, fromVersion);
            } else {
                // nah, there's no older migrator available.
                this.log.info("%s can not migrate to %s, move database file to create new database instead!", getName(), fromVersion);
                throw new RuntimeException("Database version " + fromVersion
                                           + " is not supported, please, switch to the supported database or create the new one.");
            }
        }

        this.log.info("Performing migration to %s", getVersion());
        performMigration(dbManager);
    }

    @Override
    public String readCurrentVersion(IDBManager dbManager) {
        return dbManager.execute(statement -> tableExists(statement, INFO_TABLE) ? readVersion(statement) : null);
    }

    @Override
    public void create(IDBManager dbManager) {
        this.log.info("Clearing existing database structure...");
        clearDatabase(dbManager);

        this.log.info("Creating database schema v%s...", getVersion());
        dbManager.execute(statement -> createSchema(statement));
    }

    protected boolean canMigrateFrom(String fromVersion) {
        return this.previousVersion == null || this.previousVersion.getVersion().equals(fromVersion);
    }

    protected Object createSchema(Statement statement) throws SQLException {
        createSchemaTable(statement);
        return null;
    }

    protected abstract void performMigration(IDBManager dbManager);

    private String readVersion(Statement statement) throws SQLException {
        try (ResultSet set = statement.executeQuery("SELECT SchemaVersion FROM " + INFO_TABLE + ";")) {
            return set.next() ? set.getString(1) : null;
        }
    }

    private boolean tableExists(Statement statement, String infoTable) throws SQLException {
        try (ResultSet tables = statement.executeQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='" + infoTable + "';")) {
            return tables.next();
        }
    }

    private void clearDatabase(IDBManager dbManager) {
        List<String> tables = dbManager.execute(statement -> listTables(statement));

        for (String table : tables) {
            dbManager.execute(statement -> removeTable(statement, table));
        }
    }

    private List<String> listTables(Statement statement) throws SQLException {
        List<String> result = new ArrayList<>();

        ResultSet tablesSet = statement.executeQuery("SELECT name FROM sqlite_master WHERE type='table'");

        while (tablesSet.next())
            result.add(tablesSet.getString(1));

        return result;
    }

    private Object removeTable(Statement statement, String tableName) throws SQLException {
        statement.execute("DROP TABLE IF EXISTS " + tableName + ";");
        return null;
    }

    private void createSchemaTable(Statement statement) throws SQLException {
        statement.execute("CREATE TABLE " + INFO_TABLE + "(SchemaVersion TEXT);");
        statement.executeUpdate("INSERT INTO " + INFO_TABLE + " VALUES ('" + getVersion() + "')");
    }
}
