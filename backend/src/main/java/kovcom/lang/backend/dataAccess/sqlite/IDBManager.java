package kovcom.lang.backend.dataAccess.sqlite;

import java.sql.*;
import java.util.*;

public interface IDBManager {

    <T> T execute(ICommand<T> command);

    <T> List<T> query(String query, IMapper<T> mapper, Object... params);

    <T> T querySingle(String query, IMapper<T> mapper, Object... params);

    <T> T querySingleOrDefault(String query, IMapper<T> mapper, Object... params);

    public interface ICommand<T> {
         T run(Statement statement) throws SQLException;
    }

    public interface IMapper<T>{
        T map(ResultSet result) throws SQLException;
    }
}
